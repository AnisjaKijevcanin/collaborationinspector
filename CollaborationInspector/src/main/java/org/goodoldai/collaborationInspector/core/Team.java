package org.goodoldai.collaborationInspector.core;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jgit.lib.Repository;
import org.goodoldai.collaborationInspector.exceptions.TeamException;

/**
 * This class represents a (programming) team. Each team should have two or more
 * team members, and a repository where they commit their code.
 * 
 * @author Bojan Tomic
 *
 */
public class Team {

	/**
	 * Code repository URI for this team.
	 */
	private Repository repository;

	/**
	 * List with team members
	 */
	private LinkedList<TeamMember> members;

	/**
	 * Returns the team code repository.
	 * 
	 * @return the repository
	 */

	public Repository getRepository() {
		return repository;
	}

	/**
	 * Sets the team code repository.
	 * 
	 * @param repository
	 *            the repository to set
	 * @throws org.goodoldai.collaborationinspector.exceptions.TeamException
	 *             if the repository is null
	 */
	public void setRepository(Repository repository) {
		if (repository == null)
			throw new TeamException("The repository must not be null");

		this.repository = repository;
	}

	/**
	 * Returns the list with team members.
	 * 
	 * @return the members
	 */
	public LinkedList<TeamMember> getMembers() {
		return members;
	}

	/**
	 * Sets the list with team members.
	 * 
	 * @param members
	 *            the members to set
	 * @throws org.goodoldai.collaborationinspector.exceptions.TeamException
	 *             if the list is null
	 */
	public void setMembers(LinkedList<TeamMember> members) {
		if (members == null)
			throw new TeamException("The list with team members must not be null");

		this.members = members;
	}

	private List<DiffInformation> allFilesChangedTotal;
	private CommitInformation informationAllCommits;
	private int numberOfFilesWithCode;
	private String uri;

	public List<DiffInformation> getAllFilesChangedTotal() {
		return allFilesChangedTotal;
	}

	public void setAllFilesChangedTotal(List<DiffInformation> allFilesChangedTotal) {
		this.allFilesChangedTotal = allFilesChangedTotal;
	}

	public CommitInformation getInformationAllCommits() {
		return informationAllCommits;
	}

	public void setInformationAllCommits(CommitInformation informationAllCommits) {
		this.informationAllCommits = informationAllCommits;
	}
	
	public int getNumberOfFilesWithCode() {
		return numberOfFilesWithCode;
	}

	public void setNumberOfFilesWithCode(int numberOfFilesWithCode) {
		this.numberOfFilesWithCode = numberOfFilesWithCode;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void calculateAllChangedFilesTotal() {
		ArrayList<DiffInformation> files = new ArrayList<DiffInformation>();
		for (TeamMember member : members) {
			member.calculateAllChangedFilesTotal();
			List<DiffInformation> filesMember = member.getAllFilesChangedTotal();
			for (DiffInformation diffInformationMember : filesMember) {
				if (files.contains(diffInformationMember)) {
					int i = files.indexOf(diffInformationMember);
					DiffInformation newDiff = files.get(i).addTwoDiffs(diffInformationMember);
					files.remove(i);
					files.add(newDiff);
				} else {
					files.add(diffInformationMember);
				}
			}
		}
		setAllFilesChangedTotal(files);
		for (DiffInformation diffInformation : files) {
			if (diffInformation.getLinesAdded() != 0)
				numberOfFilesWithCode++;
		}
	}
}
