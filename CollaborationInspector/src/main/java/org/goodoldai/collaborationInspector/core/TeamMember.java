package org.goodoldai.collaborationInspector.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import org.goodoldai.collaborationInspector.exceptions.TeamMemberException;

import net.sourceforge.jFuzzyLogic.rule.Rule;

/**
 * This class represents a single team member with all of his/hers information
 * like user name, e-mail, alternative e-mails as well as statistics about
 * his/her involvement in the team project (total number of commits, lines of
 * code added/removed, etc.)
 * 
 * @author Bojan Tomic
 *
 */
public class TeamMember {

	/**
	 * Team member user name that he/she uses in the code repository (when
	 * making commits).
	 */
	private String userName = null;

	/**
	 * Team member e-mail that he/she uses in the code repository (when making
	 * commits).
	 */
	private String email = null;

	/**
	 * Total number of commits for this team member.
	 */
	private int totalCommits = 0;

	/**
	 * A list of commits that were made by this team member.
	 */
	private LinkedList<Commit> commits = null;
	/**
	 * Total number of lines added and deleted by a team member.
	 */

	/**
	 * Returns the team member user name (that he/she uses in the repository
	 * when committing)
	 * 
	 * @return team member user name
	 */

	private CommitInformation commitInformationTotal;
	private List<DiffInformation> allFilesChangedTotal;
	private int numberOfAppearances;
	private boolean bronzeBadge;
	private boolean silverBadge;
	private boolean goldBadge;

	private double percentageDoc;
	private double percentageGUI;
	private double percentageTest;
	private double percentageDomainClasses;

	private double percentageTotalAdded;
	private double percentageTotalDeleted;
	private double percentageOfFilesAltered;
	private int numberOfFilesWithCode;
	private List<Rule> activatedRulesBronze = new LinkedList<>();
	private List<Rule> activatedRulesSilver = new LinkedList<>();
	private List<Rule> activatedRulesGold = new LinkedList<>();
	private double bronzeBadgeDouble;
	private String explanation = "";

	public int getNumberOfAppearances() {
		return numberOfAppearances;
	}

	public void setNumberOfAppearances(int numberOfAppearances) {
		this.numberOfAppearances = numberOfAppearances;
	}

	public double getBronzeBadgeDouble() {
		return bronzeBadgeDouble;
	}

	public void setBronzeBadgeDouble(double bronzeBadgeDouble) {
		this.bronzeBadgeDouble = bronzeBadgeDouble;
	}

	public double getSilverBadgeDouble() {
		return silverBadgeDouble;
	}

	public void setSilverBadgeDouble(double silverBadgeDouble) {
		this.silverBadgeDouble = silverBadgeDouble;
	}

	public double getGoldBadgeDouble() {
		return goldBadgeDouble;
	}

	public void setGoldBadgeDouble(double goldBadgeDouble) {
		this.goldBadgeDouble = goldBadgeDouble;
	}

	private double silverBadgeDouble;
	private double goldBadgeDouble;

	public List<Rule> getActivatedRulesBronze() {
		return activatedRulesBronze;
	}

	public void setActivatedRulesBronze(List<Rule> activatedRulesBronze) {
		this.activatedRulesBronze = activatedRulesBronze;
	}

	public List<Rule> getActivatedRulesSilver() {
		return activatedRulesSilver;
	}

	public void setActivatedRulesSilver(List<Rule> activatedRulesSilver) {
		this.activatedRulesSilver = activatedRulesSilver;
	}

	public List<Rule> getActivatedRulesGold() {
		return activatedRulesGold;
	}

	public void setActivatedRulesGold(List<Rule> activatedRulesGold) {
		this.activatedRulesGold = activatedRulesGold;
	}

	public double getPercentageDoc() {
		return percentageDoc;
	}

	public void setPercentageDoc(double percentageDoc) {
		this.percentageDoc = percentageDoc;
	}

	public double getPercentageGUI() {
		return percentageGUI;
	}

	public void setPercentageGUI(double percentageGUI) {
		this.percentageGUI = percentageGUI;
	}

	public double getPercentageTest() {
		return percentageTest;
	}

	public void setPercentageTest(double percentageTest) {
		this.percentageTest = percentageTest;
	}

	public double getPercentageDomainClasses() {
		return percentageDomainClasses;
	}

	public void setPercentageDomainClasses(double percentageDomainClasses) {
		this.percentageDomainClasses = percentageDomainClasses;
	}

	public boolean isBronzeBadge() {
		return bronzeBadge;
	}

	public void setBronzeBadge(boolean bronzeBadge) {
		this.bronzeBadge = bronzeBadge;
	}

	public boolean isSilverBadge() {
		return silverBadge;
	}

	public void setSilverBadge(boolean silverBadge) {
		this.silverBadge = silverBadge;
	}

	public boolean isGoldBadge() {
		return goldBadge;
	}

	public void setGoldBadge(boolean goldBadge) {
		this.goldBadge = goldBadge;
	}

	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the team member user name (that he/she uses in the repository when
	 * committing). The new value must not be null nor an empty String.
	 * 
	 * @param userName
	 *            the user name to set
	 * @throws org.goodoldai.collaborationinspector.exceptions.TeamMemberException
	 *             if the entered user name is null or an empty String.
	 */
	public void setUserName(String userName) {
		if (userName == null || userName.isEmpty())
			throw new TeamMemberException("The user name must not be null nor empty.");

		this.userName = userName;
	}

	/**
	 * Returns the team member e-mail (that he/she uses in the repository when
	 * committing)
	 *
	 * @return team member email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the team member e-mail (that he/she uses in the repository when
	 * committing). The new value must not be null, must contain at least three
	 * characters, and must contain the @ character
	 * 
	 * @param email
	 *            the e-mail to set
	 * @throws org.goodoldai.collaborationinspector.exceptions.TeamMemberException
	 *             if the entered email is not in the correct format
	 * @see #checkEmailFormat(java.lang.String)
	 */
	public void setEmail(String email) {
		if (!checkEmailFormat(email))
			throw new TeamMemberException("'" + email + "' - is not the correct format for an email");

		this.email = email;
	}

	/**
	 * Returns the total number of commits for this team member
	 * 
	 * @return the total number of commits for this team member
	 */
	public int getTotalCommits() {
		return totalCommits;
	}

	/**
	 * Sets the total number of commits for this team member
	 * 
	 * @param totalCommits
	 *            the total commits to set
	 * @throws org.goodoldai.collaborationinspector.exceptions.TeamMemberException
	 *             if the entered number of commits is less than zero
	 */
	public void setTotalCommits(int totalCommits) {
		if (totalCommits < 0)
			throw new TeamMemberException("The number of commits must be zero or larger.");

		this.totalCommits = totalCommits;
	}

	/**
	 * Returns a list of team member's commits
	 * 
	 * @return the commits
	 */
	public LinkedList<Commit> getCommits() {
		return commits;
	}

	/**
	 * Sets the list of team member's commits
	 * 
	 * @param commits
	 *            the commits to set
	 */
	public void setCommits(LinkedList<Commit> commits) {
		this.commits = commits;
	}

	public CommitInformation getCommitInformationTotal() {
		return commitInformationTotal;
	}

	public void setCommitInformationTotal(CommitInformation commitInformationTotal) {
		this.commitInformationTotal = commitInformationTotal;
	}

	public List<DiffInformation> getAllFilesChangedTotal() {
		return allFilesChangedTotal;
	}

	public void setAllFilesChangedTotal(List<DiffInformation> allFilesChangedTotal) {
		this.allFilesChangedTotal = allFilesChangedTotal;
	}

	public double getPercentageTotalAdded() {
		return percentageTotalAdded;
	}

	public void setPercentageTotalAdded(double percentageTotalAdded) {
		this.percentageTotalAdded = percentageTotalAdded;
	}

	public double getPercentageTotalDeleted() {
		return percentageTotalDeleted;
	}

	public void setPercentageTotalDeleted(double percentageTotalDeleted) {
		this.percentageTotalDeleted = percentageTotalDeleted;
	}

	public double getPercentageOfFilesAltered() {
		return percentageOfFilesAltered;
	}

	public void setPercentageOfFilesAltered(double percentageOfFilesAltered) {
		this.percentageOfFilesAltered = percentageOfFilesAltered;
	}

	public double getNumberOfFilesWithCode() {
		return numberOfFilesWithCode;
	}

	public void setNumberOfFilesWithCode(int numberOfFilesWithCode) {
		this.numberOfFilesWithCode = numberOfFilesWithCode;
	}
	
	public String getExplanation() {
		return explanation;
	}
	
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	/**
	 * Method that checks if the email String is in the right format: #@####
	 * 
	 * @param email
	 *            email String to check
	 * @return true if the format is ok, otherwise false
	 */
	private boolean checkEmailFormat(String email) {
		if (email == null || email.length() < 3 || email.indexOf('@') == -1)
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "TeamMember [userName=" + userName + ", email=" + email + "\n totalCommits=" + totalCommits
				+ ", total lines added= " + getCommitInformationTotal().getLinesAdded() + ", total lines deleted= "
				+ getCommitInformationTotal().getLinesDeleted() + ", total lines empty added= "
				+ getCommitInformationTotal().getLinesEmptyAdded() + ", total lines empty deleted= "
				+ getCommitInformationTotal().getLinesEmptyDeleted() + "\npercentageDoc=" + percentageDoc
				+ ", percentageGUI=" + percentageGUI + ", percentageTest=" + percentageTest
				+ ", percentageDomainClasses=" + percentageDomainClasses + ", \npercentageTotalAdded="
				+ percentageTotalAdded + ", percentageTotalDeleted=" + percentageTotalDeleted + "\nnumberOfAppearances="
				+ numberOfAppearances + "\nbronzeBadge=" + bronzeBadge + " " + bronzeBadgeDouble + ", silverBadge="
				+ silverBadge + " " + silverBadgeDouble + ", goldBadge=" + goldBadge + " " + goldBadgeDouble + "]";
	}

	private ArrayList<DiffInformation> getAllChangedFiles() {
		ArrayList<DiffInformation> files = new ArrayList<DiffInformation>();
		for (int i = 0; i < commits.size(); i++) {
			for (int j = 0; j < commits.get(i).getDiffEntryList().size(); j++) {
				files.add(commits.get(i).getDiffEntryList().get(j));
			}
		}
		return files;
	}

	public void calculateAllChangedFilesTotal() {
		ArrayList<DiffInformation> filesTotal = new ArrayList<DiffInformation>();
		ArrayList<DiffInformation> files = getAllChangedFiles();

		for (DiffInformation diffInformation : files) {
			if (filesTotal.contains(diffInformation)) {
				int i = filesTotal.indexOf(diffInformation);
				DiffInformation newDiff = filesTotal.get(i).addTwoDiffs(diffInformation);
				filesTotal.remove(i);
				filesTotal.add(newDiff);
			} else {
				filesTotal.add(diffInformation);
			}
		}
		setAllFilesChangedTotal(filesTotal);
		for (DiffInformation diffInformation : filesTotal) {
			if (diffInformation.getLinesAdded() != 0)
				numberOfFilesWithCode++;
		}
	}

	public List<DiffInformation> calculateTopFiveFiles() {
		calculateAllChangedFilesTotal();
		List<DiffInformation> list = new LinkedList<>();
		for (DiffInformation diffInformation : allFilesChangedTotal) {
			if (diffInformation.getFile().contains(".java"))
				list.add(diffInformation);
		}
		Collections.sort(list, new Comparator<DiffInformation>() {
			public int compare(DiffInformation di1, DiffInformation di2) {
				if (di1.getLinesAdded() > di2.getLinesAdded())
					return -1;
				else if (di1.getLinesAdded() < di2.getLinesAdded())
					return 1;
				else
					return 0;
			}
		});
		if (list.size() < 5)
			return list;
		return list.subList(0, 5);
	}

	public double[] getRolePercentages() {
		double[] roles = new double[4];
		double documenting = 0;
		double gui = 0;
		double testing = 0;
		double remaining = 0;
		if (commitInformationTotal.getLinesDocumentingAdded() != 0)
			documenting = commitInformationTotal.getLinesDocumentingAdded() * 100.0
					/ commitInformationTotal.getLinesAdded();
		if (commitInformationTotal.getLinesGUIAdded() != 0)
			gui = commitInformationTotal.getLinesGUIAdded() * 100.0 / commitInformationTotal.getLinesAdded();
		if (commitInformationTotal.getLinesTestingAdded() != 0)
			testing = commitInformationTotal.getLinesTestingAdded() * 100.0 / commitInformationTotal.getLinesAdded();
		if (commitInformationTotal.getLinesRemainingAdded() != 0)
			remaining = commitInformationTotal.getLinesRemainingAdded() * 100.0
					/ commitInformationTotal.getLinesAdded();
		roles[0] = documenting;
		roles[1] = gui;
		roles[2] = testing;
		roles[3] = remaining;
		System.out
				.println("Doc: " + documenting + ",gui: " + gui + ",testing: " + testing + ",remaining: " + remaining);
		return roles;
	}

	public void mergeMembers(TeamMember member) {
		userName += ", " + member.getUserName();
		email += ", " + member.getEmail();
		totalCommits += member.getTotalCommits();
		commits.addAll(member.getCommits());
		commitInformationTotal.addInformation(member.getCommitInformationTotal());
		numberOfAppearances += member.getNumberOfAppearances();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TeamMember) {
			if (userName.equals(((TeamMember) obj).getUserName()) && email.equals(((TeamMember) obj).getEmail()))
				return true;
		}
		return false;
	}
}
