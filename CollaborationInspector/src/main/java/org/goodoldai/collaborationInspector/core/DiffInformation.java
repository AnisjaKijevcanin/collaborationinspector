package org.goodoldai.collaborationInspector.core;

public class DiffInformation {
	private String file;
	private int linesAdded;
	private int linesDeleted;
	private int linesDocumentingAdded;
	private int linesDocumentingDeleted;
	private int linesGUIAdded;
	private int linesGUIDeleted;
	private int linesTestingAdded;
	private int linesTestingDeleted;
	private int linesRemainingAdded;
	private int linesRemainingDeleted;
	private int linesEmptyAdded;
	private int linesEmptyDeleted;

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getLinesAdded() {
		return linesAdded;
	}

	public void setLinesAdded(int linesAdded) {
		this.linesAdded = linesAdded;
	}

	public int getLinesDeleted() {
		return linesDeleted;
	}

	public void setLinesDeleted(int linesDeleted) {
		this.linesDeleted = linesDeleted;
	}

	public int getLinesDocumentingAdded() {
		return linesDocumentingAdded;
	}

	public void setLinesDocumentingAdded(int linesDocumentingAdded) {
		this.linesDocumentingAdded = linesDocumentingAdded;
	}

	public int getLinesDocumentingDeleted() {
		return linesDocumentingDeleted;
	}

	public void setLinesDocumentingDeleted(int linesDocumentingDeleted) {
		this.linesDocumentingDeleted = linesDocumentingDeleted;
	}

	public int getLinesGUIAdded() {
		return linesGUIAdded;
	}

	public void setLinesGUIAdded(int linesGUIAdded) {
		this.linesGUIAdded = linesGUIAdded;
	}

	public int getLinesGUIDeleted() {
		return linesGUIDeleted;
	}

	public void setLinesGUIDeleted(int linesGUIDeleted) {
		this.linesGUIDeleted = linesGUIDeleted;
	}

	public int getLinesTestingAdded() {
		return linesTestingAdded;
	}

	public void setLinesTestingAdded(int linesTestingAdded) {
		this.linesTestingAdded = linesTestingAdded;
	}

	public int getLinesTestingDeleted() {
		return linesTestingDeleted;
	}

	public void setLinesTestingDeleted(int linesTestingDeleted) {
		this.linesTestingDeleted = linesTestingDeleted;
	}

	public int getLinesRemainingAdded() {
		return linesRemainingAdded;
	}

	public void setLinesRemainingAdded(int linesRemainingAdded) {
		this.linesRemainingAdded = linesRemainingAdded;
	}

	public int getLinesRemainingDeleted() {
		return linesRemainingDeleted;
	}

	public void setLinesRemainingDeleted(int linesRemainingDeleted) {
		this.linesRemainingDeleted = linesRemainingDeleted;
	}

	public int getLinesEmptyAdded() {
		return linesEmptyAdded;
	}

	public void setLinesEmptyAdded(int linesEmptyAdded) {
		this.linesEmptyAdded = linesEmptyAdded;
	}

	public int getLinesEmptyDeleted() {
		return linesEmptyDeleted;
	}

	public void setLinesEmptyDeleted(int linesEmptyDeleted) {
		this.linesEmptyDeleted = linesEmptyDeleted;
	}

	public DiffInformation addTwoDiffs(DiffInformation info) {
		DiffInformation newDiff = new DiffInformation();
		newDiff.setFile(info.getFile());
		newDiff.setLinesAdded(linesAdded + info.getLinesAdded());
		newDiff.setLinesDeleted(linesDeleted + info.getLinesDeleted());
		newDiff.setLinesDocumentingAdded(linesDocumentingAdded + info.getLinesDocumentingAdded());
		newDiff.setLinesDocumentingDeleted(linesDocumentingDeleted + info.getLinesDocumentingDeleted());
		newDiff.setLinesEmptyAdded(linesEmptyAdded + info.getLinesEmptyAdded());
		newDiff.setLinesEmptyDeleted(linesEmptyDeleted + info.getLinesEmptyDeleted());
		newDiff.setLinesGUIAdded(linesGUIAdded + info.getLinesGUIAdded());
		newDiff.setLinesGUIDeleted(linesGUIDeleted + info.getLinesGUIDeleted());
		newDiff.setLinesRemainingAdded(linesRemainingAdded + info.getLinesRemainingAdded());
		newDiff.setLinesRemainingDeleted(linesRemainingDeleted + info.getLinesRemainingDeleted());
		newDiff.setLinesTestingAdded(linesTestingAdded + info.getLinesTestingAdded());
		newDiff.setLinesTestingDeleted(linesTestingDeleted + info.getLinesTestingDeleted());
		return newDiff;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		DiffInformation diffInfo = (DiffInformation) obj;
		if (diffInfo.getFile().equals(file))
			return true;
		return false;
	}

	@Override
	public String toString() {
		return file + ",added: " + linesAdded + ",deleted: " + linesDeleted;
	}

}
