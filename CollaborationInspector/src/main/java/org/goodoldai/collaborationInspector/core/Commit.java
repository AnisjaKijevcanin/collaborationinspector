package org.goodoldai.collaborationInspector.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.jgit.revwalk.RevCommit;

public class Commit {
	private RevCommit revCommit;
	private List<DiffInformation> diffEntryList = new ArrayList<DiffInformation>();
	private Date commitDate;
	private CommitInformation linesInfo;

	public RevCommit getRevCommit() {
		return revCommit;
	}

	public void setRevCommit(RevCommit revCommit) {
		this.revCommit = revCommit;
	}

	public List<DiffInformation> getDiffEntryList() {
		return diffEntryList;
	}

	public void setDiffEntryList(List<DiffInformation> diffEntryList) {
		this.diffEntryList = diffEntryList;
	}

	public Date getCommitDate() {
		return commitDate;
	}

	public void setCommitDate(Date commitDate) {
		this.commitDate = commitDate;
	}

	public CommitInformation getLinesInfo() {
		return linesInfo;
	}

	public void setLinesInfo(CommitInformation linesInfo) {
		this.linesInfo = linesInfo;
	}

}
