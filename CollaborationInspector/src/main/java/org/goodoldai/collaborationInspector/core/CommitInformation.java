package org.goodoldai.collaborationInspector.core;

public class CommitInformation {
	private int linesAdded;
	private int linesDeleted;
	private int linesDocumentingAdded;
	private int linesDocumentingDeleted;
	private int linesGUIAdded;
	private int linesGUIDeleted;
	private int linesTestingAdded;
	private int linesTestingDeleted;
	private int linesRemainingAdded;
	private int linesRemainingDeleted;
	private int linesEmptyAdded;
	private int linesEmptyDeleted;

	public int getLinesEmptyAdded() {
		return linesEmptyAdded;
	}

	public void setLinesEmptyAdded(int linesEmptyAdded) {
		this.linesEmptyAdded = linesEmptyAdded;
	}

	public int getLinesEmptyDeleted() {
		return linesEmptyDeleted;
	}

	public void setLinesEmptyDeleted(int linesEmptyDeleted) {
		this.linesEmptyDeleted = linesEmptyDeleted;
	}

	public int getLinesAdded() {
		return linesAdded;
	}

	public void setLinesAdded(int linesAdded) {
		this.linesAdded = linesAdded;
	}

	public int getLinesDeleted() {
		return linesDeleted;
	}

	public void setLinesDeleted(int linesDeleted) {
		this.linesDeleted = linesDeleted;
	}

	public int getLinesDocumentingAdded() {
		return linesDocumentingAdded;
	}

	public void setLinesDocumentingAdded(int linesDocumentingAdded) {
		this.linesDocumentingAdded = linesDocumentingAdded;
	}

	public int getLinesDocumentingDeleted() {
		return linesDocumentingDeleted;
	}

	public void setLinesDocumentingDeleted(int linesDocumentingDeleted) {
		this.linesDocumentingDeleted = linesDocumentingDeleted;
	}

	public int getLinesGUIAdded() {
		return linesGUIAdded;
	}

	public void setLinesGUIAdded(int linesGUIAdded) {
		this.linesGUIAdded = linesGUIAdded;
	}

	public int getLinesGUIDeleted() {
		return linesGUIDeleted;
	}

	public void setLinesGUIDeleted(int linesGUIDeleted) {
		this.linesGUIDeleted = linesGUIDeleted;
	}

	public int getLinesTestingAdded() {
		return linesTestingAdded;
	}

	public void setLinesTestingAdded(int linesTestingAdded) {
		this.linesTestingAdded = linesTestingAdded;
	}

	public int getLinesTestingDeleted() {
		return linesTestingDeleted;
	}

	public void setLinesTestingDeleted(int linesTestingDeleted) {
		this.linesTestingDeleted = linesTestingDeleted;
	}

	public int getLinesRemainingAdded() {
		return linesRemainingAdded;
	}

	public void setLinesRemainingAdded(int linesRemainingAdded) {
		this.linesRemainingAdded = linesRemainingAdded;
	}

	public int getLinesRemainingDeleted() {
		return linesRemainingDeleted;
	}

	public void setLinesRemainingDeleted(int linesRemainingDeleted) {
		this.linesRemainingDeleted = linesRemainingDeleted;
	}

	public void addInformation(CommitInformation infoOneCommit) {
		linesAdded += infoOneCommit.getLinesAdded();
		linesDeleted += infoOneCommit.getLinesDeleted();
		linesDocumentingAdded += infoOneCommit.getLinesDocumentingAdded();
		linesDocumentingDeleted += infoOneCommit.getLinesDocumentingDeleted();
		linesGUIAdded += infoOneCommit.getLinesGUIAdded();
		linesGUIDeleted += infoOneCommit.getLinesGUIDeleted();
		linesTestingAdded += infoOneCommit.getLinesTestingAdded();
		linesTestingDeleted += infoOneCommit.getLinesTestingDeleted();
		linesRemainingAdded += infoOneCommit.getLinesRemainingAdded();
		linesRemainingDeleted += infoOneCommit.getLinesRemainingDeleted();
		linesEmptyAdded += infoOneCommit.getLinesEmptyAdded();
		linesEmptyDeleted += infoOneCommit.getLinesEmptyDeleted();
	}

	@Override
	public String toString() {
		return "\n[linesAdded=" + linesAdded + ", linesDeleted=" + linesDeleted + "\nlinesDocumentingAdded="
				+ linesDocumentingAdded + ", linesDocumentingDeleted=" + linesDocumentingDeleted + "\nlinesGUIAdded="
				+ linesGUIAdded + ", linesGUIDeleted=" + linesGUIDeleted + "\nlinesTestingAdded=" + linesTestingAdded
				+ ", linesTestingDeleted=" + linesTestingDeleted + "\nlinesRemainingAdded=" + linesRemainingAdded
				+ ", linesRemainingDeleted=" + linesRemainingDeleted + "\nlinesEmptyAdded=" + linesEmptyAdded
				+ ", linesEmptyDeleted=" + linesEmptyDeleted + "]";
	}

}
