package org.goodoldai.collaborationInspector.exceptions;

public class TeamException extends RuntimeException {

	private static final long serialVersionUID = 7312255252951868775L;
	
	public TeamException() {
		super();
	}

	public TeamException(String message) {
		super(message);
	}

	public TeamException(Throwable cause) {
		super(cause);
	}

	public TeamException(String message, Throwable cause) {
		super(message, cause);
	}

	public TeamException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
