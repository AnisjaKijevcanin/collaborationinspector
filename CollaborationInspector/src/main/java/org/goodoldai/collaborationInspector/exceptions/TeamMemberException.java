package org.goodoldai.collaborationInspector.exceptions;

public class TeamMemberException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6424601199784513001L;

	public TeamMemberException() {
		super();
	}

	public TeamMemberException(String message) {
		super(message);
	}

	public TeamMemberException(Throwable cause) {
		super(cause);
	}

	public TeamMemberException(String message, Throwable cause) {
		super(message, cause);
	}

	public TeamMemberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
