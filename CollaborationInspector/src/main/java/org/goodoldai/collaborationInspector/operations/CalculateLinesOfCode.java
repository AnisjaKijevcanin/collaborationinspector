package org.goodoldai.collaborationInspector.operations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.revwalk.RevWalk;
import org.goodoldai.collaborationInspector.core.Commit;
import org.goodoldai.collaborationInspector.core.CommitInformation;
import org.goodoldai.collaborationInspector.core.DiffInformation;
import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class CalculateLinesOfCode {
	
	public static void execute(Team team) throws MissingObjectException, IncorrectObjectTypeException, IOException, GitAPIException{
		
		RevWalk  revWalk = new RevWalk(team.getRepository());
		CommitInformation informationAllCommits = new CommitInformation();
		team.setInformationAllCommits(informationAllCommits);
		for (int i = 0; i < team.getMembers().size(); i++) {
			team.getMembers().get(i).setCommitInformationTotal(new CommitInformation());
			for (int j = 0; j < team.getMembers().get(i).getCommits().size(); j++) {
				Commit commit = team.getMembers().get(i).getCommits().get(j);
				commit.setDiffEntryList(new ArrayList<DiffInformation>());
				CommitInformation commitInfo = new CommitInformation();
				if(commit.getRevCommit().getShortMessage().toUpperCase().contains("MERGE")){
					team.getMembers().get(i).setTotalCommits(team.getMembers().get(i).getTotalCommits()-1);
					continue;
				}
					
				if(commit.getRevCommit().getShortMessage().toUpperCase().contains("REVERT")){
					clearRevertedCommit(commit, team.getMembers().get(i));
					continue;
				}
				if(commit.getRevCommit().getParentCount() == 0) {
					commitInfo = AnalyzeFirstCommit.execute(team.getRepository(), commit);
				}
				else {
					List<DiffEntry> diffs = GetDiffFilesBetweenCommits.execute(team, revWalk, commit.getRevCommit());
					if(diffs != null) 
						commitInfo = CalculateInformationForOneDiff.execute(diffs, team, commit);
				}
				commit.setLinesInfo(commitInfo);
				
				team.getMembers().get(i).getCommitInformationTotal().addInformation(commitInfo);
			}
			team.getInformationAllCommits().addInformation(team.getMembers().get(i).getCommitInformationTotal());
		}
		team.calculateAllChangedFilesTotal();
	}
	
	private static void clearRevertedCommit(Commit commit, TeamMember member){
		String message = commit.getRevCommit().getShortMessage().toUpperCase();
		int index = message.indexOf("REVERT");
		int count = 0;
		while (index != -1) {
		    count++;
		    message = message.substring(index + 1);
		    index = message.indexOf("REVERT");
		}
		/*if(count % 2 != 0){
			for (int i = 0; i < member.getCommits().size(); i++) {
				if(commit.getRevCommit().getShortMessage().contains(member.getCommits().get(i).getRevCommit().getShortMessage())){
					member.getCommits().remove(i);
					member.getCommits().remove(commit);
					member.setTotalCommits(member.getTotalCommits()-2);
				}
			}
		}
		else {*/
			for (int i = 0; i < member.getCommits().size(); i++) {
				if(commit.getRevCommit().getShortMessage().contains(member.getCommits().get(i).getRevCommit().getShortMessage())){
					member.getCommits().remove(commit);
					member.setTotalCommits(member.getTotalCommits()-1);
					if(member.getCommits().get(i).getRevCommit().getShortMessage().toUpperCase().contains("REVERT")){
						member.getCommits().remove(i);
						member.setTotalCommits(member.getTotalCommits()-1);
					} else {
						if(count % 2 != 0){
							member.getCommits().remove(i);
							member.setTotalCommits(member.getTotalCommits()-1);
						}							
					}
					
				}
			}
		/*}*/
		
	}
}

