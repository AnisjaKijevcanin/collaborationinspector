package org.goodoldai.collaborationInspector.operations;

import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.goodoldai.collaborationInspector.core.Team;

public class GetDiffFilesBetweenCommits {
	public static List<DiffEntry> execute(Team team, RevWalk revWalk, RevCommit commit)
			throws IncorrectObjectTypeException, IOException, GitAPIException {
		List<DiffEntry> diffs = null;

		if (commit.getParentCount() == 1) {
			RevCommit parent = commit.getParent(0);
			// a new reader to read objects
			ObjectReader reader = team.getRepository().newObjectReader();
			// constructs a parser for the old tree
			CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
			// Reset this parser to walk through the given tree.
			oldTreeIter.reset(reader, parent.getTree());
			// constructs a parser for the new tree
			CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
			// Reset this parser to walk through the given tree.
			newTreeIter.reset(reader, commit.getTree());
			// Git instance for using it's methods
			Git git = new Git(team.getRepository());

			// Executes the Diff command with all the options and parameters
			// collected by the setter methods
			// returns changes between commits
			diffs = git.diff().setNewTree(newTreeIter).setOldTree(oldTreeIter).call();
			git.close();
			return diffs;
		}
		return diffs;
	}
}
