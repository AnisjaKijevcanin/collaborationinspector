package org.goodoldai.collaborationInspector.operations;


import org.goodoldai.collaborationInspector.core.DiffInformation;
import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class CalculatePercentagesPerFile {
	public static void execute(Team team){
		for(TeamMember member: team.getMembers()){
			for (DiffInformation infoMember : member.getAllFilesChangedTotal()) {
				int i = team.getAllFilesChangedTotal().indexOf(infoMember);
					System.out.println("file: " + infoMember.getFile() + " percentage: " + (infoMember.getLinesAdded()*100.0)/team.getAllFilesChangedTotal().get(i).getLinesAdded());
			}	
		}
	}
}	



