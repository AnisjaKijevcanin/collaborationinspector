package org.goodoldai.collaborationInspector.operations;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.goodoldai.collaborationInspector.core.Team;

public class InitializeRepository {
			//TODO Anisja initialize repo from JGIT in this method by using the provided URI
			//The initialized repo shoud be set as a Team attribute
			
	public static void execute(Team team, URI repositoryURI) throws InvalidRemoteException, TransportException, GitAPIException, IOException{
		
		//prepares FileRepositoryBuilder to open resulting repository
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		Repository repository = builder
			.readEnvironment() // scan environment GIT_* variables
			.findGitDir() // scan up the file system tree
			.build();
			 
			//prepares a folder for new repository
			File localPath = File.createTempFile("CollaborationInspectorRepo", "");
			localPath.delete();
			    
			Git git = Git.cloneRepository() .setURI( repositoryURI.toString() ).setDirectory( localPath ) .call();
			repository=git.getRepository();	
			
			team.setRepository(repository);
	}

}
