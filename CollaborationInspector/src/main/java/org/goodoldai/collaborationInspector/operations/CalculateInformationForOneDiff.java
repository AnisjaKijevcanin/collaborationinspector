package org.goodoldai.collaborationInspector.operations;

import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.diff.DiffEntry;
import org.goodoldai.collaborationInspector.core.Commit;
import org.goodoldai.collaborationInspector.core.CommitInformation;
import org.goodoldai.collaborationInspector.core.DiffInformation;
import org.goodoldai.collaborationInspector.core.Team;

public class CalculateInformationForOneDiff {
	public static CommitInformation execute(List<DiffEntry> diffs, Team team, Commit commit) throws IOException {
		CommitInformation info = new CommitInformation();

		int linesAdded = 0;
		int linesDeleted = 0;
		int linesDocumentingAdded = 0;
		int linesTestingAdded = 0;
		int linesEmptyAdded = 0;
		int linesGUIAdded = 0;
		int linesDocumentingDeleted = 0;
		int linesTestingDeleted = 0;
		int linesEmptyDeleted = 0;
		int linesGUIDeleted = 0;
		int linesRemainingAdded = 0;
		int linesRemainingDeleted = 0;

		for (DiffEntry diff : diffs) {

			if (diff.getNewPath().contains(".java")) {
				DiffInformation diffInfo = new DiffInformation();
				diffInfo.setFile(diff.getNewPath());
				int linesAddedDiff = 0;
				int linesDeletedDiff = 0;
				int linesDocumentingAddedDiff = 0;
				int linesTestingAddedDiff = 0;
				int linesEmptyAddedDiff = 0;
				int linesGUIAddedDiff = 0;
				int linesDocumentingDeletedDiff = 0;
				int linesTestingDeletedDiff = 0;
				int linesEmptyDeletedDiff = 0;
				int linesGUIDeletedDiff = 0;
				int linesRemainingAddedDiff = 0;
				int linesRemainingDeletedDiff = 0;

				boolean doesDiffContainGUICode = false;
				boolean doesDiffContainTestCode = false;
				// ArrayList<String> lines = new ArrayList<String>();
				if (CheckFileForContent.executeTESTFile(diff.toString()))
					doesDiffContainTestCode = true;
				if (CheckFileForContent.executeGUIFile(diff.toString()))
					doesDiffContainGUICode = true;
				String text = GetStringFromDiffEntry.execute(team, diff);
				// for (int i = 0; i < lines.size(); i++) {
				String[] linesOfDiffText = text.split("\n");

				for (int j = 0; j < linesOfDiffText.length; j++) {
					if (linesOfDiffText[j].startsWith("+") && !linesOfDiffText[j].startsWith("++")) {
						linesAdded++;
						linesAddedDiff++;
						boolean check = true;
						if (doesDiffContainTestCode) {
							linesTestingAdded++;
							linesTestingAddedDiff++;
							check = false;
						}
						if (doesDiffContainGUICode) {
							linesGUIAdded++;
							linesTestingAddedDiff++;
							check = false;
						}
						if (check) {
							linesRemainingAdded++;
							linesRemainingAddedDiff++;
						}
						if (CheckFileForContent.executeDOCLine(linesOfDiffText[j])) {
							linesDocumentingAdded++;
							linesDocumentingAddedDiff++;
						}
						if (linesOfDiffText[j].matches("[+-]\\s*")) {
							linesEmptyAdded++;
							linesEmptyAddedDiff++;
						}
					}
					if (linesOfDiffText[j].startsWith("-") && !linesOfDiffText[j].startsWith("--")) {
						linesDeleted++;
						linesDeletedDiff++;
						boolean check = true;
						if (doesDiffContainTestCode) {
							linesTestingDeleted++;
							linesTestingDeletedDiff++;
							check = false;
						}
						if (doesDiffContainGUICode) {
							linesGUIDeleted++;
							linesGUIDeletedDiff++;
							check = false;
						}
						if (check) {
							linesRemainingDeleted++;
							linesRemainingDeletedDiff++;
						}
						if (CheckFileForContent.executeDOCLine(linesOfDiffText[j])) {
							linesDocumentingDeleted++;
							linesDocumentingDeletedDiff++;
						}
						if (linesOfDiffText[j].matches("[+-]\\s*")) {
							linesEmptyDeleted++;
							linesEmptyDeletedDiff++;
						}
					}
				}
				diffInfo.setLinesAdded(linesAddedDiff);
				diffInfo.setLinesDeleted(linesDeletedDiff);
				diffInfo.setLinesDocumentingAdded(linesDocumentingAddedDiff);
				diffInfo.setLinesDocumentingDeleted(linesDocumentingDeletedDiff);
				diffInfo.setLinesEmptyAdded(linesEmptyAddedDiff);
				diffInfo.setLinesEmptyDeleted(linesEmptyDeletedDiff);
				diffInfo.setLinesGUIAdded(linesGUIAddedDiff);
				diffInfo.setLinesGUIDeleted(linesGUIDeletedDiff);
				diffInfo.setLinesTestingAdded(linesTestingAddedDiff);
				diffInfo.setLinesTestingDeleted(linesTestingDeletedDiff);
				diffInfo.setLinesRemainingAdded(linesRemainingAddedDiff);
				diffInfo.setLinesRemainingDeleted(linesRemainingDeletedDiff);

				commit.getDiffEntryList().add(diffInfo);

				/*
				 * if(commit.getRevCommit().getShortMessage().
				 * equals("Revert \"Namesteno da ne moze da se menja izbor kategorije nakon odabira prvog slova.\""
				 * ) || commit.getRevCommit().getShortMessage().
				 * equals("Namesteno da ne moze da se menja izbor kategorije nakon odabira prvog slova."
				 * )){ System.out.println(diffInfo.getLinesAdded() +
				 * diffInfo.getLinesDeleted()); } }
				 */
			}
		}
			info.setLinesAdded(linesAdded);
			info.setLinesDeleted(linesDeleted);
			info.setLinesDocumentingAdded(linesDocumentingAdded);
			info.setLinesDocumentingDeleted(linesDocumentingDeleted);
			info.setLinesEmptyAdded(linesEmptyAdded);
			info.setLinesEmptyDeleted(linesEmptyDeleted);
			info.setLinesGUIAdded(linesGUIAdded);
			info.setLinesGUIDeleted(linesGUIDeleted);
			info.setLinesTestingAdded(linesTestingAdded);
			info.setLinesTestingDeleted(linesTestingDeleted);
			info.setLinesRemainingAdded(linesRemainingAdded);
			info.setLinesRemainingDeleted(linesRemainingDeleted);

			return info;

		}
}
