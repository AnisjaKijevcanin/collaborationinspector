package org.goodoldai.collaborationInspector.operations;

import java.util.List;

import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class MergeMembers {
	
	public static void execute(Team team, List<Integer> indexes){
		TeamMember member = team.getMembers().get(indexes.get(0));
		for (int i = indexes.size() - 1; i > 0; i--) {
			member.mergeMembers(team.getMembers().get(indexes.get(i)));
			int index = indexes.get(i);
			System.out.println(indexes.get(i));
			team.getMembers().remove(index);
		}
		
		for (TeamMember m : team.getMembers()) {
			System.out.println(m.getUserName());
		}
		
	}
}
