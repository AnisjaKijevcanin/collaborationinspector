package org.goodoldai.collaborationInspector.operations;


public class CheckFileForContent {
	
	public static boolean executeGUIFile(String s) {
		if(s.toUpperCase().contains("GUI")) {
			return true;
		}
		return false;
	}
	
	public static boolean executeDOCLine(String s) {
		if(s.contains("    * ") || s.contains("/**") || s.contains("*/"))
			return true;
		return false;
	}
	
	public static boolean executeTESTFile(String s) {
		if(s.toUpperCase().contains("TEST")) {
			return true;
		}
		return false;
	}
	
	public static boolean executeEmptyLineDiff(String s) {
		if(s.matches("[+-]\\s*"))
			return true;
		return false;
	}
	
	public static boolean executeEmptyLine(String s) {
		if(s.matches("\\s*"))
			return true;
		return false;
	}
	
}
