package org.goodoldai.collaborationInspector.operations;


import org.goodoldai.collaborationInspector.core.Team;

public class CalculateMemberStatistics {
	public static void execute(Team team) {
		CheckDates.execute(team);
		CalculatePercentagesPerMember.execute(team);
	}
}
