package org.goodoldai.collaborationInspector.operations;

import java.io.IOException;
import java.util.LinkedList;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.goodoldai.collaborationInspector.core.Commit;
import org.goodoldai.collaborationInspector.core.Team;

public class CalculateCommits {

	public static void execute(Team team) throws NoHeadException, GitAPIException, IOException {

		// using Git instance retrieves all commits from repository
		Git git = new Git(team.getRepository());
		Iterable<RevCommit> commits = git.log().all().call();
		git.close();
		// goes through all commits, assigning each one to a member, adding
		// commits to lists of commits
		// by each member and counting them simultaneously
		for (RevCommit commit : commits) {
			for (int i = 0; i < team.getMembers().size(); i++) {
				if (commit.getAuthorIdent().getEmailAddress().equals(team.getMembers().get(i).getEmail())
						&& commit.getAuthorIdent().getName().equals(team.getMembers().get(i).getUserName())) {
					if (team.getMembers().get(i).getCommits() == null) {
						LinkedList<Commit> commitsByMember = new LinkedList<Commit>();
						team.getMembers().get(i).setCommits(commitsByMember);
					}
					Commit newCommit = new Commit();
					newCommit.setRevCommit(commit);
					newCommit.setCommitDate(commit.getAuthorIdent().getWhen());
					team.getMembers().get(i).getCommits().add(newCommit);
					team.getMembers().get(i).setTotalCommits(team.getMembers().get(i).getTotalCommits() + 1);
				}

			}
		}
	}

}
