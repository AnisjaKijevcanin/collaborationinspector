package org.goodoldai.collaborationInspector.operations;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.goodoldai.collaborationInspector.core.Commit;
import org.goodoldai.collaborationInspector.core.CommitInformation;
import org.goodoldai.collaborationInspector.core.DiffInformation;

public class AnalyzeFirstCommit {

	public static CommitInformation execute(Repository repo, Commit commit)
			throws MissingObjectException, IncorrectObjectTypeException, CorruptObjectException, IOException {
		CommitInformation info = new CommitInformation();

		int linesAdded = 0;
		int linesDocumentingAdded = 0;
		int linesTestingAdded = 0;
		int linesEmptyAdded = 0;
		int linesGUIAdded = 0;
		int linesRemainingAdded = 0;

		RevTree tree = commit.getRevCommit().getTree();
		TreeWalk walk = new TreeWalk(repo);

		// ArrayList<String> allLines = new ArrayList<String>();
		String[] linesSplit = null;
		walk.addTree(tree.getId());
		walk.setRecursive(true);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		while (walk.next()) {

			DiffInformation diffInfo = new DiffInformation();
			int linesAddedDiff = 0;
			int linesDocumentingAddedDiff = 0;
			int linesTestingAddedDiff = 0;
			int linesEmptyAddedDiff = 0;
			int linesGUIAddedDiff = 0;
			int linesRemainingAddedDiff = 0;
			String changedFile = walk.getPathString();
			diffInfo.setFile(changedFile);
			ObjectId objectId = walk.getObjectId(0);
			ObjectLoader loader = repo.open(objectId);
			byte[] bytes = loader.getBytes();
			out.reset();
			out.write(bytes);
			String lines = out.toString("UTF-8");
			linesSplit = lines.split("\n");
			if (changedFile.contains(".java")) {
				linesAdded += linesSplit.length;
				linesAddedDiff += linesSplit.length;

				boolean check = true;
				if (CheckFileForContent.executeGUIFile(changedFile)) {
					check = false;
					linesGUIAdded += linesSplit.length;
					linesGUIAddedDiff += linesSplit.length;
				}
				if (CheckFileForContent.executeTESTFile(changedFile)) {
					check = false;
					linesTestingAdded += linesSplit.length;
					linesTestingAddedDiff += linesSplit.length;
				}
				if (check) {
					linesRemainingAdded += linesSplit.length;
					linesRemainingAddedDiff += linesSplit.length;
				}

				for (int i = 0; i < linesSplit.length; i++) {
					if (CheckFileForContent.executeDOCLine(linesSplit[i])) {
						linesDocumentingAdded++;
						linesDocumentingAddedDiff++;
					}
					if (CheckFileForContent.executeEmptyLine(linesSplit[i])) {
						linesEmptyAdded++;
						linesEmptyAddedDiff++;
					}
				}
			}
			diffInfo.setLinesAdded(linesAddedDiff);
			diffInfo.setLinesDocumentingAdded(linesDocumentingAddedDiff);
			diffInfo.setLinesGUIAdded(linesGUIAddedDiff);
			diffInfo.setLinesTestingAdded(linesTestingAddedDiff);
			diffInfo.setLinesEmptyAdded(linesEmptyAddedDiff);
			diffInfo.setLinesRemainingAdded(linesRemainingAddedDiff);
			commit.getDiffEntryList().add(diffInfo);

		}

		// if all lines come into play, this is where I would set them
		info.setLinesAdded(linesAdded);
		info.setLinesDocumentingAdded(linesDocumentingAdded);
		info.setLinesGUIAdded(linesGUIAdded);
		info.setLinesTestingAdded(linesTestingAdded);
		info.setLinesEmptyAdded(linesEmptyAdded);
		info.setLinesRemainingAdded(linesRemainingAdded);
		commit.setLinesInfo(info);

		walk.close();
		out.close();

		return info;
	}
}
