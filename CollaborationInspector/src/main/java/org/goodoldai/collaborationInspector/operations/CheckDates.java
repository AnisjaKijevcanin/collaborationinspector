package org.goodoldai.collaborationInspector.operations;



import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;


import org.goodoldai.collaborationInspector.core.Commit;
import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class CheckDates {
	static LinkedList<Commit> sortedCommits;
	public static void execute(Team team){
		sortedCommits = new LinkedList<Commit>();
		sortCommits(team);
		Collections.sort(sortedCommits, new Comparator<Commit>() {
			public int compare(Commit o1, Commit o2) {
				if(o1.getCommitDate().getTime() < o2.getCommitDate().getTime())
					return -1;
				else if(o1.getCommitDate().getTime() > o2.getCommitDate().getTime())
					return 1;
				else
					return 0;
				
			}
		});
		
		for (TeamMember member : team.getMembers()) {
			int numberOfAppearances = 0;
			for (int i = 0; i < sortedCommits.size()-1; i++) {	
				if(member.getUserName().contains(sortedCommits.get(i).getRevCommit().getAuthorIdent().getName()) &&
						!member.getUserName().contains(sortedCommits.get(i+1).getRevCommit().getAuthorIdent().getName()))
					numberOfAppearances++;
				if(member.getUserName().contains(sortedCommits.get(i+1).getRevCommit().getAuthorIdent().getName()) &&
						i == sortedCommits.size()-2)
					numberOfAppearances++;
			}
			member.setNumberOfAppearances(numberOfAppearances);
		}
		
	}
	
	public static void sortCommits(Team team){
		for (TeamMember member : team.getMembers()) {
			for (Commit commit : member.getCommits()) {
				sortedCommits.add(commit);
			}
		}
	}
}
