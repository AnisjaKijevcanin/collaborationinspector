package org.goodoldai.collaborationInspector.operations;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class InitializeRepositoryAndTeam {
	
	public static Team execute(String repositoryURI) throws URISyntaxException, IOException, InvalidRemoteException, TransportException, GitAPIException, MalformedURLException{
		URL url = new URL(repositoryURI);
		URI uri = null;
		uri = url.toURI();
		// initializes new team
		Team team = new Team();
		// prepares FileRepositoryBuilder to open resulting repository
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		Repository repository = builder.readEnvironment().findGitDir().build();
		// prepares a folder for new repository
		File localPath = File.createTempFile("CollaborationInspectorRepo", "");
		localPath.delete();

		Git git = Git.cloneRepository().setURI(uri.toString()).setDirectory(localPath).call();
		repository = git.getRepository();
		team.setUri(repositoryURI);
		// sets the repo in team
		team.setRepository(repository);

		Iterable<RevCommit> commits = git.log().all().call();
		LinkedList<TeamMember> members = new LinkedList<TeamMember>();
		// goes through commits, finds names and emails of committers and
		// add them to members list if the member is already not in the list
		for (RevCommit commit : commits) {
			//System.out.println(commit.getShortMessage() + commit.getCommitterIdent());
			PersonIdent author = commit.getAuthorIdent();
			if (members.isEmpty() || isTheMemberInTheList(author, members) == false) {
				TeamMember member = new TeamMember();
				member.setEmail(author.getEmailAddress());
				member.setUserName(author.getName());
				members.add(member);
			}
		}
		team.setMembers(members);
		git.close();
		return team;
	}

	// goes through the members, checks for duplicates
	public static boolean isTheMemberInTheList(PersonIdent person, LinkedList<TeamMember> members) {
		for (int i = 0; i < members.size(); i++) {
			if (members.get(i).getUserName().equals(person.getName())
					&& members.get(i).getEmail().equals(person.getEmailAddress())) {
				return true;
			}

		}
		return false;
	}

}
