package org.goodoldai.collaborationInspector.operations;


import java.text.DecimalFormat;

import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class CalculatePercentagesPerMember {
	public static void execute(Team team) {
		for (TeamMember member : team.getMembers()) {
			double doc = 0;
			double test = 0;
			double gui = 0;
			double rest = 0;
			double filesPercentage = 0;
			double percentageTotalAdded = 0;
			double percentageTotalDeleted = 0;
			//check for zeros earlier in the analysis
			filesPercentage = member.getNumberOfFilesWithCode()*100.0/team.getNumberOfFilesWithCode();
			percentageTotalAdded = member.getCommitInformationTotal().getLinesAdded()*100.0/team.getInformationAllCommits().getLinesAdded();
			percentageTotalDeleted = member.getCommitInformationTotal().getLinesDeleted()*100.0/team.getInformationAllCommits().getLinesDeleted();
			
			if(team.getInformationAllCommits().getLinesDocumentingAdded() != 0)
				doc = member.getCommitInformationTotal().getLinesDocumentingAdded()*100.0/team.getInformationAllCommits().getLinesDocumentingAdded();
			if(team.getInformationAllCommits().getLinesTestingAdded() != 0) 
				test = member.getCommitInformationTotal().getLinesTestingAdded()*100.0/team.getInformationAllCommits().getLinesTestingAdded();
			if(team.getInformationAllCommits().getLinesGUIAdded() != 0)
				gui = member.getCommitInformationTotal().getLinesGUIAdded()*100.0/team.getInformationAllCommits().getLinesGUIAdded();	
			if(team.getInformationAllCommits().getLinesRemainingAdded() != 0)
				rest = member.getCommitInformationTotal().getLinesRemainingAdded()*100.0/team.getInformationAllCommits().getLinesRemainingAdded();	
			
			DecimalFormat newFormat = new DecimalFormat("#.##");
			double docFormatted =  Double.valueOf(newFormat.format(doc));
			double testFormatted =  Double.valueOf(newFormat.format(test));
			double guiFormatted =  Double.valueOf(newFormat.format(gui));
			double restFormatted =  Double.valueOf(newFormat.format(rest));
			double filesPercentageFormatted = Double.valueOf(newFormat.format(filesPercentage));
			double percentageTotalAddedFormatted = Double.valueOf(newFormat.format(percentageTotalAdded));
			double percentageTotalDeletedFormatted = Double.valueOf(newFormat.format(percentageTotalDeleted));
			
			member.setPercentageDoc(docFormatted);
			member.setPercentageDomainClasses(restFormatted);
			member.setPercentageGUI(guiFormatted);
			member.setPercentageTest(testFormatted);
			member.setPercentageOfFilesAltered(filesPercentageFormatted);
			member.setPercentageTotalAdded(percentageTotalAddedFormatted);
			member.setPercentageTotalDeleted(percentageTotalDeletedFormatted);
		}
	}
}
