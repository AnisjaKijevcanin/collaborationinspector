package org.goodoldai.collaborationInspector.operations;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.goodoldai.collaborationInspector.core.Team;

public class GetStringFromDiffEntry {
	public static String execute(Team team, DiffEntry diff) {
		//ArrayList<String> diffText = new ArrayList<String>();
		//out stream where diff formatter will write to
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		//Create a new formatter with a default level of context
		DiffFormatter diffFormatter = new DiffFormatter(out);
		//Set the repository the formatter can load object contents from
		diffFormatter.setRepository( team.getRepository() );
		//sets lineCount to zero, no lines before or after the changed line are shown
		diffFormatter.setContext( 0 );
		//goes through diff list
			 try {
				//Formats a patch script for one file entry
				diffFormatter.format(diff);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 //raw text - is it even useful, why??
	         //RawText raw = new RawText(out.toByteArray());
	         //raw.getLineDelimiter();
	         //adds a string from byte stream - check if properly converted!
	         //diffText.add(out.toString());
			 String diffText = out.toString();
	         //System.out.println(out.toString() + "oneDiffDown \n");
	         out.reset();
	         diffFormatter.close();
	         return diffText;
		 }
}
