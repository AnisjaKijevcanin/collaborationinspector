package org.goodoldai.collaborationInspector;

import java.io.IOException;
import java.net.URI;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.exceptions.TeamException;
import org.goodoldai.collaborationInspector.operations.CalculateCommits;
import org.goodoldai.collaborationInspector.operations.CalculateLinesOfCode;
import org.goodoldai.collaborationInspector.operations.InitializeRepository;
import org.goodoldai.collaborationInspector.operations.InitializeRepositoryAndTeam;


/**
 * Represents a facade for the Collaboration inspector, so all functionality
 * is available through its methods. 
 * 
 * @author Bojan Tomic
 *
 */
public class CollaborationInspector {
	
	/**
	 * Programming team to be analyzed
	 */
	private Team team;
	
	/**
	 * Initializes a JGit repository by using the provided URI, and sets the team
	 * to be analyzed. This constructor should be used only when one or more team members use
	 * several alternative e-mails when doing code commits. In order for Collaboration inspector to
	 * correctly group commits to the right team members, the team and
	 * all of its members (together with alternative e-mails) must be manually set.
	 * 
	 * If this is not the case (each team member uses only one email account),
	 *  use the one argument constructor {@link org.goodoldai.collaborationInspector.CollaborationInspector#CollaborationInspector(java.net.URI)}
	 * @param team The team to be analyzed
	 * @param repositoryURI team repository URI
	 * @throws org.goodoldai.collaborationInspector.exceptions.TeamException if the entered team
	 * is null or empty, or if the repositoryURI is null
	 */
	public CollaborationInspector(Team team, URI repositoryURI){
		if (team == null)
			throw new TeamException("The team must not be null");
		
		if (team.getMembers().size() == 0)
			throw new TeamException("The team must have at least one member");
		
		if (repositoryURI == null)
			throw new TeamException("The repository URI must not be null");
		
		this.team = team;
		
		try {
			InitializeRepository.execute(this.team, repositoryURI);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Initializes a JGit repository by using the provided URI, but also
	 * the team to be analyzed. This constructor should be used only when each team member uses only
	 * one email account, because team members are identified through author identification data
	 * extracted from commits in the repository. 
	 * 
	 * If this is not the case (one or more team members use multiple email accounts/aliases while committing),
	 *  use the two argument constructor 
	 *  {@link org.goodoldai.collaborationInspector.CollaborationInspector#CollaborationInspector(org.goodoldai.collaborationInspector.core.Team, java.net.URI)}
	 * 
	 * @param repositoryURI team repository URI
	 * @throws org.goodoldai.collaborationInspector.exceptions.TeamException if the repositoryURI is null
	 */
	//change made regarding input parameters, in the future maybe changing it back to URI
	public CollaborationInspector(String repositoryURI){
		 if (repositoryURI == null)
				throw new TeamException("The repository URI must not be null");
			
		 try {
			this.team = InitializeRepositoryAndTeam.execute(repositoryURI);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	/**
	 * Returns the team to be analyzed.
	 * 
	 * @return the team
	 */
	public Team getTeam() {
		return team;
	}

	/**
	 * Calculates the number of commits for each team member.
	 */
	public void calculateCommits(){
		try {
			CalculateCommits.execute(this.team);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Calculates the number of lines of code for each team member.
	 */
	public void calculateLinesOfCode(){
		try {
			CalculateLinesOfCode.execute(this.team);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
