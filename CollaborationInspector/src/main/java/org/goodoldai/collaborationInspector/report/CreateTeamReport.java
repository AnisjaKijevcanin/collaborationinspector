package org.goodoldai.collaborationInspector.report;

import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import org.goodoldai.collaborationInspector.core.Team;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

public class CreateTeamReport {
	public static String execute(Team team) {
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_26);
		cfg.setDefaultEncoding("UTF-8");
		cfg.setClassForTemplateLoading(CreateTeamReport.class, "");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
		try {
			Template temp = cfg.getTemplate("report.ftl");
			Map<String, Object> input = new HashMap<String, Object>();
			input.put("team", team);
			input.put("members", team.getMembers());

			Writer out = new StringWriter();
			temp.process(input, out);
			String doc = out.toString();

			return doc;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
}
