<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style type="text/css">
html {
	font-family: Tahoma, Geneva, sans-serif;
	height: 100%;
	font-size: 14px;
}

body {
	background: #24478f;
	min-height: 100%;
	height: 100%;
	color: #005EC4;

}

table {
	width: 100%;
	background: #ffff80;
	border-collapse: collapse;
	border-color: blue;
}

td, th {
	border: 1px solid #dddddd;
	text-align: center;
	padding: 8px;
}

th {
	background: #ffffcc;
}

header {
	background: #ccebff;
	height:150px;
}

h4 {
	margin-top: 5px;
	margin-bottom:5px;
}
#logo {
	margin-right: 1.5%;
	margin-top: 1.5%;
	float: right;
}

#info {
	margin-left: 1.5%;
	margin-top: 2%;
	float:left;
}

#content {
	text-align: center;
}

#wrapper {
	width: 80%;
	margin-left: 10%;
	margin-right: 10%;
	background: white;
	min-height: 100%;
}

#member {
	width: 100%;
	background: #ffffcc;
	margin-top: 10px;
}

#rules {
	text-align: left;
	padding: 5px;
}

#infoT td{
	text-align: left;
}
footer {
	height: 30px;
	background: #ccebff;
	width: 80%;
	margin-left: 10%;
	margin-right: 10%;
	margin-bottom: 10px;
}

a {
	text-decoration: none;
	color: #ff9933;	
}
a:LINK {
	color: #ff9933;	
}

a:VISITED {
	color: #ff9933;
}

a:HOVER {
	color: #b35900;
}
</style>
<title>${team.uri?keep_after_last("/")}</title>
</head>
<body>
	<div id="wrapper">
	<header>
		<div id="info">
			<h1>
				Report for <a href=${team.uri} target="_blank">team repository</a>
			</h1>
		</div>
		<img id="logo" alt="logo"
			src="data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAHMAAABkCAYAAAC1kA/FAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAAGQhJREFUeF7tXQt4VNWdPzd3CD6LimDmEUhmJljZta3SqtW2lAKZZDKAq1IVMnmKft2vdru0rm1tP/Zru926W9eiJW/CU4vpan2tT5QkM5lMHkMIJD6CiqIh5EEgCQmEZO7Z///OmTCPM5OZZJIMOL/v+30kueecuZzfPf/XPfcOiSGGCwJSPVnqrBWKJbuw12kX3gI+ITWQG9nhGC4ESFZypdNBXqQfCZQ2AxuBDuAhgUqHBUlqJE9QSkTWPIZoBd1HLpFahc/ofhCvPgBBYKdDsEBbBesWQzSCNpBKeoAjoCfrgC0gqJ38nnWLIdowUkNMtI2JxRPRl9BWqiaLWPcYoglSE3le9o884XiEFey0kd+x7jFEE6QmoTPkVYmEttIhUsW6xxAtgGDmKjli5YkWiCCms1k4woaIIVog2YiaHvQRazziyoyJGX2gR8glzgbByRUtEF1ifsSGiCGaACazLSyfCZSayausewzRBMlBCoIWC3zZKtDRWvLPrHsM0QSpltwg55k84XwJKYxslveRS1j3GKINtI78AldcUHNrB/PaKki0kXyHdYshGkEpiYMV91ywkp7zoDDirBMep4QIrFsM0QxnvTDME5I2AJvIdtYshmjHqI3k0w98RPSg0yGMxnxllIMWk1mSjdwsHRL6g/rMJjn42T9QS66jm0gc6x7DTAPEuwa4ymkXSqQmoU06AKsOTSlPRDdRaAcEQS3CaalOqBu1x22S6sj3YJxL2bAxTCekJvJTEO8TCXcSYDrSAkQRwykaYFvMS98HwhjO/RAYHRQaUVj2MTFMJSBaFSS7sDuYT5w0DwMtZC37yBimCmBOC+WCejgrMFzC2OB3z0oN5Bb2sTFEGuDfHg9rJ8EkCf6XQtSbxD4+hkgBgpOH6Xv+Ez7VdLYIbewUYogEQMg06X2IUqdpRXoRt2c2EiumPex0YpgoaC1JkiPVmRASiZ8L5hb856/ZKcUwEdBGMgtShWPj5o1TTRQUomfJEpfNTi2GcCC9RmY7m4SD4+6BnS6ioO8Lp6QaspCdYgyhQmoifwvrRvN0EO+BNgp9sbpuGJDqhSI5cg3XT2J7N9E048XQBIQgRva7SPwd/477a91teWPxiG3RfzYJr7BTjSEYRhtItjzpdo9JHI8ewkkHhWHpPeGE5BCOOuuFJmedUOm0C9vg3zKpViiDf98F1kHO+pnUKnRJLcLQmMCh+mbwn0472cxOOQYepJq4PLlOyptAX+IqwX2yrprqMJi/PVI9uQvSGDUETnOkNjIbb1Szob2AJUFaQeLxabGBveQ6SD0MTofwF6lROCGX8tBPB1uxcAwCMwki3HvYkDF4Ap+bhBU1NK7Zw+OtsAKbhdPwc4VkFdeAeJexYSYF8IUK2qj4jmQXngKxjsqmPtjjDnjh1ZFvsO4xIKiFXC3fMB7PzIEpRMHBRP73VAchroJ+XL7zAJjsA4LEPR+gdEhwxLafeABSkDLZZPJWJf4N/RmsEqeD/DushATWbVogvUkuH60l94Ev7uEWL/BvtSSHNf9yQ7KTZQG3R+LEYTntIAQsNqJnXWYEaMrBp+4E8+70M714F+dtomVNv5ygLRCENAufBDKv0geCU6ojD7PmoWHTpjiyznYHyar+LVlveZ2YLW3EXPXxGDOrjwDfJebqp4Emkme9kvUMCZKDfBMi4D6vc3YQOvTINQdZky8nIIpcL79zwENAmXDlgy/qgtV4B2saHJn79CTT8muSV28nOfbTJH8/JQ8A8xop/O7PvAbqatOEv4+QXEcLMVs3k8yq29mIQYERM1iLWvnc3Wb37XjaKVxfxpp8uQCr8gpIJ7yDHpwYrLLsFwakKpLImvKxicbJIpptb5ENBykIQkmWhdEaBqG9GZhTT8mDLfB77WGSXb2cPNw2m30SF5jaOBsE+5ighwkduVs52j5HdzNr8uUB5IS/9VuVuGvOIXwMV/41rBkfKGKW3UZyG0dITq1LEK5Q4RLGybbBhVEvwdifgjlOY5/IBe7sg5SqwP0oofOleHpybsrZo4s1wc//YgP4ym6vKg+sUDBdX0D0OJ818Ufaa7Nhwn9OHmg+CxPumnyuKJMljlvjWqk5tTuJ+e257Az8ACtUdNYL1XKZD6LuQZ2W9qv1z7HDFz+cNuGPvrsGwEdK40as5ppmkg9+kCvAFBF9bJbtHFlvvZ6dBRdwcTbTA4QObryanptzPe1SJd3LDl28kKrJPNyvOiYkrE65NGYl32ZN/JH1jg6ClQ6SUwcTO1WrMRDZKn2gaRjMe8B8ku4j1zoPCGfQf56Yp6fDav3Q52q9hh2+ODEKqYZcBHCL2Qpi1gtPscP+MFsWkDxHH0SbPpM8zTRXg6AHKETN+ezM/EAt5DtSOxntX66mvSodHdDonmeHLk5ABNs/JiQQIsLD7JA/7qybS/L2n5hxIT2Jgq63/JCdoR+cDeR/R7dcSXuuTqE0MYUeS0i+OB/mHbWSR+iHTMg6COVbxXOtlZfdxA57404IOnIbjpNsjFY5kzpTxBWa5zhH7t/L3VOLAZFkEzu6LltEu9U6ek6j7+mee31YhYmoh/yfbBHeG0uyD8bR726/fS877I/c+hejTsgxgg/N299H1r16NTtbL0gdJN+ZpqE98/X0tEZPe1S6i+tdQ1Id+fZYgt1E6JqKW53k4fVfYYe9sa7yZ5B+wKRNd7ATKuG85PzWWsHO2A8jT15+4iSI2QOrc0STQrsSki+exx1G6yB0x8CnkdCHX7yRksK7ytkhb9zz2jyS3wwBT6hCsmgTI10s02EJz5O58DfXxLO2vv0Z0Qr4ktfOkxveo2BuV7Ez94L0gmgaWqilJ0BMNLeDGl0PXbw4nh2+cDFsIzfLvhKE3PWGmpLdd9FLilP5ddDs2qdJNuZ2nMnjEWusWbYRCJIagUVwERSMMdOyhWRW7yFZVR+C0JIsLG+MrOpREPpz8IftY8ys/gKODcsXCrcPY56jlZ25FyS9fvawJuVzXJko6Bkwt10q7Rvs8IULyUE2452Fl/fOp6T4n6hYZPiUHfLGvZhPHuJPmi9RmOy6j4i50kjWVlzBRgiMrL1qkm3dQLJqJZLtIRAWBcyWT+Rary8yq/YGvgAY8+BiwrsvHHQqk7YOQ0TrXp2Qe450K3VL2OELEyDmmQ57PJ1TZKSK7RlUUWp8iB3yRqblKZIbQoUHxchpPMR6hYd1Fi0EV2fGzDhaAXPVp2Qt9X8ztDkEMdGEm61HWQ8vfKFaeJME/hLFRPYCT6m13LYXBEZtcf925kMFvQSEjCtOp4ptpiFSkMGNAsFkHR2b5GDc0EIhSJr4u9bRBMt3WWCsyYqJfljOPSu/xXp54ZRad8QtJnIYzG1nQtJ/sMMXDuSbzy3kiHHPLSAkrMqSdCpuyzhMNnECgWzbHbLJ4k6YB/GuRpbNxnpNDOurbiYbmDmftJhAOViy/Zb18kK/SrcZV6RbTDS3Q2pdX+e8eeO7hmgCCKn/0Ss3jpCC1bKQim1gYotS+XcUzFV/k6NP3mR5En1cpu1fWS9vrN13Bcmy/xJW3i7gbmKuMbMj/jDXDMrjRUJMZI6d+0LFDnVKBuSakjsQQmIw1K1K+j/W5MJA1gs3blaUmlxCIp9ZTeMKU3/MDp/Hg42zYPL7XauOM1GezIXJz6pcxnp6I9fxoisdgTQFmd8EQll+w456I6umVY5UIyVmHpjtDVVK1nMMINr15zT6EU8x8edzmhTpWIKGa5qjDvHlK1LI1lXnhZTFXENJwYrbWJPzwKAkv0kKmgci0Z/mNo6S+2q/xnp6I7ehw2+M3PrX2VFvZNfURVTMB8Fsm/f5WYJTCxZcPaDSnXUL6clBte5N1iy6IW43HfISElluhNRkxRzW5Dxw702+IzQxsS6aaU9mPb2RXesynV59rBi5Hob04cgYM6shpbFADgnHIyWmywoUsJ5joIQoelTJXafUej8xaeIi2q7U3cmaRieEQsNTip0+qxIolqUPsibeMFeuC2nngCzm/rNkvZ1/nxA3c/n1wzQGBPOl+3jEfCacP+76I9RvQ3SPSnugH/ykr5gQ6cLq1B9qJEui9KnskpXfUOxePaooTvMXc6uRf7vLXL3Ra4ID0S3mvVX8zV5cMcdhpMSUI1rLId443ark5zHo8RUTfedptd7ZpVJF59dxKMrT3/UV0U2xLICYmTX/FVLwE81iyv7X9okczPkAVubfhzhiIgfA/PaqdNEX2YpFxmWKZ1kawmFAMc01j4clZrhmFiNNX7o/b4bFxBz0bKKOHkvQz2PNowAVa0WxxLBfUeYqDvAYUMwsS+grU95eWZnCenqDFwDl2Bth/H+ApP6W84Tfs21vRjSalceqDVtM5GhiCj2uTF7PmkcBnlqZrCjPAF/JFxIplqfzC+yZlh/Lk8qdJA+imDkNTvJDy9dZT2/k1H3hFUThdpPchgCpSe32iIqJ42RaGnkFe/CZrwUT8ySwT6V7hjWfeYCIbyi2ZnBFdFMsSRtmzb2RWXMnTNr4qQkyeNHgGdmEYmSMzG+GCa78CTvqjZzaVyMqJrYzV7/EenqhR6n7kBfNuomB0KBG28WazyziS9IXBfOVY9yRQUnBSv9NzuZ93yQPhFA0QMp3KWyPsJ7ekCtJtmxYkeUk2/pn+HkFO+IPcw2s4giKuQEuHHOl3/eKHVm48JJutXYAVx9PSDfR1LYrk1JZt5mDUJL2BzCxfAE9CYKT4rSlrNt54L3GXCwacCbJl+hbs2uaWc+J4f59X3VtS8HxIiVmC45jYD3H8NncRNVZtf4cT0BPYurSqdRyV/a0QlFu/CyYrxzjbli9RQZOrRQS7czKrpCCIKR8x6Nq4u+ENVt3jIkUKTFz4NwffNnvcfzj85O/NpyYMupZmw3EIY1uoPdqrX+FbLoQv2V5hmK3f7WHy3K8p5n2DqGcx8azrE/K/o43UTzmNX4IvcJ//HydZQUESudNesR8Zj13Bx6ImAX+0uuuCY94fEitGwXxZ+47scVCQ0NIJhYJq1fcteozsm2p//sI1tUsdE1cCH4TKT+qUHcCItx1spkOBowwzdWLYOX/Ub6R7DUOihlw28jbIYmJ55Jp/RHr5YUBtbbC835mMMpRbaKOe1906lFkUCp2moZCMrFIbFdudJJi0wI2gjdyHfvldII3YYGI20zkfTjWzyE1qIRJLYILohAEKgABd5P1lhpo1yMXwgPuuKsehbbt0OfYGDPh9yzrWTlI4vZxEy4+vEDu5+e+vSrdGZ5wPKLoJ1XamfmiOViVaxRbAxcJuNyWQcXS9D+yIbyxvnLjWGASMnElAzHKxXTEb5slrBrZF7N23DGA7u2VnuS18yV+htliZf8DL/QkaO/COyM84QJxVJNCOxP1OjbE9CGuLP0FRSlHsGDE9sXGs2TfUv9vYl/bEk+ybKPjr4ZoIVwceAFlQ3TsA3ydzAm19i2svfJEC0QJq0Eq3UY2zDShYnE8V6xQCFGtWJB6NxvJG5k197n8IW/yooy4es1WboWJKpWXndHo+kOJYj2JxYUuZdLLMMQ0vluoaMVyxZ41fLFCYXnGu2wkb9xju5TkNHwiBya8CYwm4gsuAvpK/abBIFWfYIR+zZvA8LGhph5xBYYHFM9OQsxdq+isIgP/ZQ7m2hvkoIY3gdFC2XrU/As7Yy98oVLNHcJbWxyhQiH0bQEzPX1iioWpKxV77uQLFQoxcCozHiUVa/nPX2TVPCpHiVhc503mTBKDHqxCLd3n7/cBvWrtnwY4IoXCIY0OzKxu+r94TixJG+QKFSq3mSgpTQ/87L/ZypL2KBI0G5hT1x7o1TK9VyUtPK3RjVskCES6YBHtUGt/wIabPohlxqcVZRyRwiH0J0+uuYoN6Q288rPsDjli5E3sTBDz1Xv3cb/rBDdv9aqSP+6boK+UzbJS28OGm14oilZ8E/JGZ8hFAx7LM2hceUYlDMeP3vB1MTn2mV+hGJDlNvaQrADbPAEnNbqfyw/X+ogUKlHMAY2ukA03zfjzD66DQGZwUmIid5gg90x9jI3qD9PLl5EcG77EiT/RU01ZyLpjZN07AV+2f0yzMC3cAoEv+zAnVSUFfF/C1KLYdBmYyVNhFw543GkKnHvKoHEQdPyCPPQ+5nb8SY808f0F8oudbC+RlW9ezk7ED3inY1CjG5joikRi3wEsyCfO1D3NgpXzFTsyJr8ykTCGiIIWpQZ9xRm5t+p2kuc46CqYY6VoKkwvjImpUd7+42R91QPsk7k4qklRD6l1p+RV5SNQuMRCe69Su4cNPQUoXjKLbE2bJxamrxRLDAaxePkK8ifTtXhILEn/fbANXGET99ruNA2R0ozxvx0P71Lk2M+QnHopouU/rOrgu/PMNaVyeTEI8NGDQZ9H9iZLqkmhxxOSN3arElefVCe/1KPSNfaotA74eXuPMnklneiGaUVJ2qPiDlMbBDkj8hNcMkG88owRcXvG+2EX2UMhmGxxl8lJSlLH3632YOMcML1mklN7SL5hjZEmCoK56bj5KWvj2vQF5rQVzGkNBji/Jhsa/B4A8sXx+Snasxrd8X6OIJMhmtt+tV7C6tFpWO3ws0wMrHBT2LBa396t0v089KLCltREsSytRfHXNbL58zOjkTCrwVgEKxRveJemPUaKHwztSjRX3kQyLU+QbBu+X68LcsFRWSQ/jt1NkUiuow9+/oBkVj5LxnmDpSd6VUmrcWLH29czUQbzvXgMi/FDav3fj2o0wb9WefZfUvWKcqMUkaBmstyaQcWtGY3X7Erjv2KGB7zJvKRxFsmuSyDrrMsgT32ImG0/lUtvyGz7T8GEmuTXnGLaU1Hhv8MgAPCZkJMq7c5hMIWTCXYiQfTR4KvfYqfGh1ie0awonQLzOVHKpj29X1FseIh722x6IHQm6NIGVfq2s5PIIyNJPAcnvl9IlcT/9kBFgfEn+EAsd1JnkmjWn12NDx59APno7dMlKvqlzsRE3Uml7hU0bdEgoifxfM6pdf2nNIt9Xlb83D2XKrZndEzYvGI/3AuEnCqf6voMSSwzfjSr2Bj4EfcIoGeh/rYTyuTaYY1+uM9nEidLFAGrPXiBnIHVhfXYidxhwXEgN6U9Sr1PbgpBj2LnqjO8R/HGJaQnYml6C6QvxWKRoQRM41Esok+JqDgm1oPBgojlxnax1PiEYgus1nD8KgdtEC50qpO+3q3UPnpapWvF7RsYQeILJXgTOVGiAIPg78BkO3oTkr7XrlRe26FOuhUumAkVHfAZzz6VzmcbDgY+O03nJiQARJ6zSlZ6fY0SpDWvu8p0nPaRJPr3XRj5wgVVklajKEr/2azS1CVkc9pXKtYSEU2lJysgLMB/UbyehIQbIKDJ7VYmPw8cwpUS7DGCSBBTjlOQP7JpGsMZtb5homKeUumeZMMwFKXpYPLDFxPai9uM3WyU86BEEMvSd8lml9cvknSfczkIux0uIAiaZpUbz33rDyt6f5lzW7v1ln9sG5mX3CJdvuA96RptO+RqxyGHG0IThTvJcQVimjGRyQyHGIH2qbTcbSZd6uRfYXDF6xeMKCb4dJ/tmU8vnwtmti9sMXG3XYnhf9go3njitkvFremH5IiU13cqCe5CgH/JzgxK9qyi8eBvV/xqKX38riXUetMNtH0BTGyClp6en0wHgf3w80ml68WFSBR2ouK6+7r7oz9E03ouUd/WOW8x9/0/R+YsvOq0Wjccju/E8QfVOmeHOuVWNsx5xBWnu0yj78QEIpq2svQBMGnBvgNEEHeYWl0mdwL+OEIUkSBoHPjbOFjBs+ECu2KLgd7+m+/TR8230leXfZ0evXExxRzSqVkkv8UZzS6+XjRcYsqA/eUxYKw+jZb2KfV7x9vX052Q/C0IuJyh+ml86UWvOrm7Ta/nzP9fll4BEW23PAHBVigewxVZntFDdqzgb2j2xJNLr4orSd8objfVQHoxrHgGfNxOENddDkSRw7UIgYjjyGQXDgZLaOp3wGfisy67gOUg7FZjb9wO0wGyy/QKec70J/LSmnxxt3HZhykp6m7lwq92X7fw+51KnQF86boupfb+UIntO9TJGR1q3bJO5YI7vrhWtSicN0F3XbcgH8ztKJrkYKKiWxjR6E99mqBfzLr6Y1ZB6hIQdEguoPPSFPzbNpMUV57RPAdEYt1CR8Xa+PgCw2qx2FAGk/4BXBT9EMCcgX+dbF+Q6zPc9P189zl4EgXDvlszJHmcnaZhGHMIRAS3YWgXC1NtikJDuaLIsFEsNHyfFLtuFEQr8EVQvcrkY+DPnfKdFA/i72COJYx+MfpmXYLgyTVXKbYZH4srNfbLO/CwRovcZgQzZawUt6SlyndTJg9Bfi/Q5qUaUpB206yi1HtBnN/AqvkrfI41rjT9IzCNJ0HAYUWJ8ayLhmEwlR/DsTZo8wa03S2Wp/+noijtR2JhmpEULL9pdqEhiRR9V0nKVl9JNs1Y1WhSaFm8OL7juuRbBtQphSdVya29Ku0piFo/BRFf6ElIvhtSmjC/ALaCiKQwbTHmcYqilbeSiqUz8/I+fGrMzU3TuAXxggIh/w+Oh/N57lxE+wAAAABJRU5ErkJggg==" />
	</header>
		<div id="content">
			<h3>Team members</h3>
			<#list members as member>
			<div id="member">
				<table >
					<tr>
						<th>Username</th>
						<th>Email</th>
						<th>Number of commits</th>
						<th>Added lines</th>
						<th>Deleted lines</th>
						<th>Percentage domain classes</th>
						<th>Percentage documenting</th>
						<th>Percentage testing</th>
						<th>Percentage GUI</th>
						<th>Percentage added</th>
						<th>Percentage deleted</th>
						<th>Number of appearances</th>
					</tr>

					<tr>
						<td style="width: 100px; max-width: 100px; word-wrap: break-word;">${member.userName}</td>
						<td style="width: 100px; max-width: 100px; word-wrap: break-word;">${member.email}</td>
						<td>${member.totalCommits}</td>
						<td>${member.commitInformationTotal.linesAdded}</td>
						<td>${member.commitInformationTotal.linesDeleted}</td>
						<td>${member.percentageDomainClasses}</td>
						<td>${member.percentageDoc}</td>
						<td>${member.percentageTest}</td>
						<td>${member.percentageGUI}</td>
						<td>${member.percentageTotalAdded}</td>
						<td>${member.percentageTotalDeleted}</td>
						<td>${member.numberOfAppearances}</td>						
					</tr>
				</table>
				<div id="rules">
				<h4>Activated fuzzy rules (rule number, degree of support, rule, weight)</h4>
				<div id="gold">
						<b style="color: #ffcc00;">Gold badge &#8594; </b>
						<b style="color: ${member.goldBadge?then("#00994d", "#e60000")}">${member.goldBadge?then("YES",
							"NO")}</b>
						<br>
						<#list member.activatedRulesGold as ruleG> ${ruleG}<br>
						</#list>
					</div>
					
					<div id="silver">
						<b style="color: #999999;">Silver badge &#8594; </b>
						<b style="color: ${member.silverBadge?then("#00994d", "#e60000")}">${member.silverBadge?then("YES",
							"NO")}</b>
						<br>
						<#list member.activatedRulesSilver as ruleS> ${ruleS}<br>
						</#list>
					</div>
					<div id="bronze">
						<b style="color: #996600;">Bronze badge &#8594; </b>
						<b style="color: ${member.bronzeBadge?then("#00994d", "#e60000")}">${member.bronzeBadge?then("YES",
							"NO")}</b>
						<br>
						<#list member.activatedRulesBronze as ruleB> ${ruleB}<br>
						</#list>
					</div>	
					
						<#if (member.explanation?has_content)> 
						<div id="explanation"> <br> <b> Explanation </b> <br>
						${member.explanation}
						</div>	
						</#if>
					
				</div>
				
				<table id="infoT">
					<td style="width:90%;">
					<h4>Worked mostly on files (linesAdded):</h4>
					<#list member.calculateTopFiveFiles() as diffInfo > 
    					${diffInfo.file} (${diffInfo.linesAdded})<br>
					</#list>
					</td>
					<td style="width:10%;">
					<h4>First commit: </h4>
					${member.commits[member.commits?size-1].commitDate?date}
					<h4>Last commit: </h4>
					${member.commits[0].commitDate?date}
					</td>
				</table>	
			</div>
			</#list>
		</div>
		<br>
	</div>
	<footer></footer>
</body>
</html>