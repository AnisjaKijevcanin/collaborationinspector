package org.goodoldai.collaborationInspector.assessment;

import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class BadgesCalculate {
	public static void execute(Team team) {
		if (TeamGatewayBronzeBadge.execute(team)) {
			for (TeamMember member : team.getMembers()) {
				if (MemberGatewayBronzeBadge.execute(member)) {
					BronzeBadgeCalculate.execute(member);
				}
				else {
					member.setExplanation("The member has more than 30% of empty code.");
				}
			}
			for (TeamMember member : team.getMembers()) {
				if (member.isBronzeBadge() == true)
					SilverBadgeCalculate.execute(member);
			}
			int silverCounter = RuleOfTwoCheck.execute(team, 2);
			for (TeamMember member : team.getMembers()) {
				if (silverCounter < 2){
					if(member.isSilverBadge())
						member.setExplanation(member.getExplanation() + "The team member does not receive the silver badge"
								+ " because there has to be at least one other team member that has earned the silver badge./n" );
					member.setSilverBadge(false);
				}
				else {
					if (member.isSilverBadge() == true)
						GoldBadgeCalculate.execute(member, silverCounter);
				}
			}

			if (RuleOfTwoCheck.execute(team, 3) < 2) {
				for (TeamMember member : team.getMembers()) {
					if(member.isGoldBadge())
						member.setExplanation(member.getExplanation() + "The team member does not receive the gold badge"
								+ " because there has to be at least one other team member that has earned the gold badge.");
					member.setGoldBadge(false);
				}

			}
		} else {
			System.out.println("No one gets a badge.");
		}

	}
}
