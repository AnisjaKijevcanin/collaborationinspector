package org.goodoldai.collaborationInspector.assessment;

import org.goodoldai.collaborationInspector.core.TeamMember;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.rule.Rule;

public class BronzeBadgeCalculate {
	public static double execute(TeamMember member) {
		FIS fis3 = FIS.load("fcl_rules/level3.fcl",true);
		fis3.setVariable("number_of_lines", member.getCommitInformationTotal().getLinesAdded() - member.getCommitInformationTotal().getLinesDocumentingAdded());
		fis3.setVariable("number_of_commits", member.getTotalCommits());
		fis3.evaluate();
		double bronzeBadge = fis3.getVariable("bronze_badge").getValue();
		member.setBronzeBadgeDouble(bronzeBadge);
		if(bronzeBadge > 0.75)
			member.setBronzeBadge(true);
		else {
			member.setExplanation(member.getExplanation() + "The member has not received the bronze badge and therefore is not"
					+ " eligible for further evaluation.");
		}
		 for( Rule r : fis3.getFunctionBlock("level3").getFuzzyRuleBlock("No1").getRules() )
			 if(r.getDegreeOfSupport() > 0)
				 member.getActivatedRulesBronze().add(r);
		 
		return bronzeBadge;
	}
}
