package org.goodoldai.collaborationInspector.assessment;

import org.goodoldai.collaborationInspector.core.TeamMember;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.rule.Rule;

public class GoldBadgeCalculate {
	public static double execute(TeamMember member, int numberOfMembers) {
		FIS fis5 = FIS.load("fcl_rules/level5.fcl",true);
		fis5.setVariable("percentage_added", member.getPercentageTotalAdded());
		fis5.setVariable("percentage_deleted", member.getPercentageTotalDeleted());
		fis5.setVariable("number_of_appearances", member.getNumberOfAppearances());
		fis5.setVariable("number_of_members", numberOfMembers);
		fis5.evaluate();
		double goldBadge = fis5.getVariable("gold_badge").getValue();
		member.setGoldBadgeDouble(goldBadge);
		
		if(goldBadge > 0.75)
			member.setGoldBadge(true);
		for( Rule r : fis5.getFunctionBlock("level5").getFuzzyRuleBlock("No3").getRules() )
			 if(r.getDegreeOfSupport() > 0)
				 member.getActivatedRulesGold().add(r);
		System.out.println(member + " " + member.getGoldBadgeDouble());
		return goldBadge;
	}
}
