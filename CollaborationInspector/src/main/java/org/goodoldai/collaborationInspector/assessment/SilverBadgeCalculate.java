package org.goodoldai.collaborationInspector.assessment;

import org.goodoldai.collaborationInspector.core.TeamMember;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.rule.Rule;

public class SilverBadgeCalculate {
	public static double execute(TeamMember member) {
		FIS fis4 = FIS.load("fcl_rules/level4.fcl", true);
		fis4.setVariable("percentage_doc", member.getPercentageDoc());
		fis4.setVariable("percentage_test", member.getPercentageTest());
		fis4.setVariable("percentage_gui", member.getPercentageGUI());
		fis4.setVariable("percentage_domain_classes", member.getPercentageDomainClasses());
		fis4.evaluate();
		double silverBadge = fis4.getVariable("silver_badge").getValue();
		member.setSilverBadgeDouble(silverBadge);
		if(silverBadge > 0.75)
			member.setSilverBadge(true);
		
		for( Rule r : fis4.getFunctionBlock("level4").getFuzzyRuleBlock("No2").getRules() )
			 if(r.getDegreeOfSupport() > 0)
				 member.getActivatedRulesSilver().add(r);
		return silverBadge;
	}
}
