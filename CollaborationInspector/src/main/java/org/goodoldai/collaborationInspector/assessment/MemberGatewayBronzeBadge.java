package org.goodoldai.collaborationInspector.assessment;

import org.goodoldai.collaborationInspector.core.TeamMember;

public class MemberGatewayBronzeBadge {
	public static boolean execute(TeamMember member){
		double percentageEmpty = 100;
		if(member.getCommitInformationTotal().getLinesAdded() != 0)
			percentageEmpty = (member.getCommitInformationTotal().getLinesEmptyAdded()/member.getCommitInformationTotal().getLinesAdded())*100;
		if(percentageEmpty > 30) {
			return false;
		}
		return true;		
	}
}
