package org.goodoldai.collaborationInspector.assessment;


import org.goodoldai.collaborationInspector.core.Team;


public class TeamGatewayBronzeBadge {
	
	public static boolean execute(Team team){
		if(team.getMembers().size() == 1)
			return false;
		return true;
			
	}
}
