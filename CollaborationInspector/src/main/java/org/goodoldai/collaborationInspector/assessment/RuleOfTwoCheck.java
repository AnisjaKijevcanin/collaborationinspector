package org.goodoldai.collaborationInspector.assessment;

import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.core.TeamMember;

public class RuleOfTwoCheck {
	
	public static int execute(Team team, int badge){
		int counter = 0;
		for (TeamMember member: team.getMembers()) {
			if(badge == 1){
				if(member.isBronzeBadge())
					counter++;
			}
			if(badge == 2){
				if(member.isSilverBadge()) 
					counter++;
			}
			if(badge == 3){
				if(member.isGoldBadge()) 
					counter++;
			}
		}
		return counter;
	}
}
