package org.goodoldai.collaborationInspector.gui;

import java.awt.CardLayout;
import java.awt.Toolkit;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;
import org.goodoldai.collaborationInspector.core.TeamMember;
import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.SwingWorker;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JTextArea;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentAdapter;
import java.awt.Font;

public class CollaborationInspectorGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6012629151143186671L;
	private JPanel contentPane;
	private JLabel lblLogo;
	private JLabel lblInputUrlTo;
	private JButton btnNewButton;
	private JTextField txtURI;
	boolean waiting = true;
	private JPanel oneTeamPanel;
	private JPanel waitingPanel;
	private JLabel lblWait;
	private JPanel membersPanel;
	private JLabel lblchooseTwoOr;
	private JButton btnMerge;
	private JScrollPane scrollPane;
	private JLabel lblCommittersInRepository;
	private JButton btnContinue;
	private JPanel committers;
	private List<JRadioButton> radioButtons;
	private JPanel endPanel;
	private JLabel lblSuccess;
	private JButton btnNewTeam;
	private JPanel beginPanel;
	private JLabel logoBig;
	private JButton btnEvaluateOneTeam;
	private JMenu menu;
	private JMenuItem menuItem;
	private JButton btnEvaluateMultipleTeams;
	private JPanel multipleTeamsPanel;
	private JLabel logoSmall;
	private JLabel lblInputUrlsTo;
	private JButton btnEvaluateTeams;
	private JScrollPane scrollPane_1;
	private JTextArea textArea;
	private JMenuBar menuBar;
	private JLabel lblBack;
	private SwingWorker<Void, Void> worker;

	public CollaborationInspectorGUI() {
		createContents();
	}

	private void createContents() {
		setResizable(false);
		setTitle("Collaboration Inspector");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 650, 450);
		setJMenuBar(getJMenuBar());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setIconImage(Toolkit.getDefaultToolkit().getImage(CollaborationInspectorGUI.class.getResource("/logo.png")));
		contentPane.setLayout(new CardLayout(0, 0));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Controller.closeTheApplication();
			}
		});
		contentPane.add(getBeginPanel(), "begin");
		contentPane.add(getMultipleTeamsPanel(), "multiple");
		contentPane.add(getOneTeamPanel(), "single");
		contentPane.add(getWaitingPanel(), "waiting");
		contentPane.add(getMembersPanel(), "members");
		contentPane.add(getEndPanel(), "end");
		beginPanel.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				lblBack.setVisible(false);
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				lblBack.setVisible(true);
			}
		});
		endPanel.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				lblBack.setVisible(false);
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				lblBack.setVisible(true);
			}
		});
		waitingPanel.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				lblBack.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/cancel.png")));
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				lblBack.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/back1.png")));
			}
		});
		arrange();
	}

	private void arrange() {
		btnMerge.setEnabled(false);
	}

	private JLabel getLblLogo() {
		if (lblLogo == null) {
			lblLogo = new JLabel("");
			lblLogo.setHorizontalTextPosition(SwingConstants.CENTER);
			lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
			lblLogo.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/logo.png")));
			lblLogo.setPreferredSize(new Dimension(115, 100));
		}
		return lblLogo;
	}

	private JLabel getLblInputUrlTo() {
		if (lblInputUrlTo == null) {
			lblInputUrlTo = new JLabel("Input URI to team repository:");
		}
		return lblInputUrlTo;
	}

	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Evaluate");
			btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String uri = txtURI.getText().trim();
					try {
						if (uri.isEmpty())
							throw new RuntimeException("Your input is not a valid URI.");
						CardLayout cl = (CardLayout) (contentPane.getLayout());
						cl.show(contentPane, "waiting");
						evaluate(uri);
					} catch (RuntimeException e) {
						JOptionPane.showMessageDialog(contentPane, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE,
								new ImageIcon(CollaborationInspectorGUI.class.getResource("/errors.png")));
						txtURI.setText(null);
					}
				}
			});
		}
		return btnNewButton;
	}

	public void evaluate(String uri) {
		worker = new SwingWorker<Void, Void>() {
			boolean finished = false;

			@Override
			protected Void doInBackground() {
				try {
					Controller.evaluateTeam(uri);
					finished = true;
				} catch (Exception e) {
					JOptionPane.showMessageDialog(contentPane, e.getClass().toString() + ":" + e.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE,
							new ImageIcon(CollaborationInspectorGUI.class.getResource("/errors.png")));
					e.printStackTrace();
					txtURI.setText(null);
				}
				return null;
			}

			@Override
			protected void done() {
				if (finished == true) {
					updateScrollPane();
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "members");
				} else {
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "single");
				}
			}
		};
		worker.execute();
	}

	private JTextField getTxtURI() {
		if (txtURI == null) {
			txtURI = new JTextField();
			txtURI.setFont(new Font("Monospaced", Font.PLAIN, 13));
			txtURI.setColumns(10);
		}
		return txtURI;
	}

	private JPanel getOneTeamPanel() {
		if (oneTeamPanel == null) {
			oneTeamPanel = new JPanel();
			GridBagLayout gbl_oneTeamPanel = new GridBagLayout();
			gbl_oneTeamPanel.columnWidths = new int[] { 634, 0 };
			gbl_oneTeamPanel.rowHeights = new int[] { 85, 100, 85, 30, 30, 40, 0 };
			gbl_oneTeamPanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
			gbl_oneTeamPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
			oneTeamPanel.setLayout(gbl_oneTeamPanel);
			GridBagConstraints gbc_lblLogo = new GridBagConstraints();
			gbc_lblLogo.insets = new Insets(0, 0, 5, 0);
			gbc_lblLogo.anchor = GridBagConstraints.NORTH;
			gbc_lblLogo.gridx = 0;
			gbc_lblLogo.gridy = 1;
			oneTeamPanel.add(getLblLogo(), gbc_lblLogo);
			GridBagConstraints gbc_lblInputUrlTo = new GridBagConstraints();
			gbc_lblInputUrlTo.fill = GridBagConstraints.VERTICAL;
			gbc_lblInputUrlTo.insets = new Insets(0, 0, 5, 0);
			gbc_lblInputUrlTo.gridx = 0;
			gbc_lblInputUrlTo.gridy = 3;
			oneTeamPanel.add(getLblInputUrlTo(), gbc_lblInputUrlTo);
			GridBagConstraints gbc_txtURI = new GridBagConstraints();
			gbc_txtURI.fill = GridBagConstraints.BOTH;
			gbc_txtURI.insets = new Insets(0, 0, 5, 0);
			gbc_txtURI.gridx = 0;
			gbc_txtURI.gridy = 4;
			oneTeamPanel.add(getTxtURI(), gbc_txtURI);
			GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
			gbc_btnNewButton.fill = GridBagConstraints.BOTH;
			gbc_btnNewButton.gridx = 0;
			gbc_btnNewButton.gridy = 5;
			oneTeamPanel.add(getBtnNewButton(), gbc_btnNewButton);
		}
		return oneTeamPanel;
	}

	private JPanel getWaitingPanel() {
		if (waitingPanel == null) {
			waitingPanel = new JPanel();
			waitingPanel.setLayout(null);
			waitingPanel.add(getLblWait());
		}
		return waitingPanel;
	}

	private JLabel getLblWait() {
		if (lblWait == null) {
			lblWait = new JLabel("");
			lblWait.setBounds(262, 140, 110, 100);
			lblWait.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/rolling.gif")));
		}
		return lblWait;
	}

	private JPanel getMembersPanel() {
		if (membersPanel == null) {
			membersPanel = new JPanel();
			GridBagLayout gbl_membersPanel = new GridBagLayout();
			gbl_membersPanel.columnWidths = new int[] { 447, 187 };
			gbl_membersPanel.rowHeights = new int[] { 15, 30, 230, 45, 40 };
			gbl_membersPanel.columnWeights = new double[] { 0.0, 0.0 };
			gbl_membersPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
			membersPanel.setLayout(gbl_membersPanel);
			GridBagConstraints gbc_lblCommittersInRepository = new GridBagConstraints();
			gbc_lblCommittersInRepository.gridwidth = 2;
			gbc_lblCommittersInRepository.insets = new Insets(0, 0, 5, 0);
			gbc_lblCommittersInRepository.gridx = 0;
			gbc_lblCommittersInRepository.gridy = 1;
			membersPanel.add(getLblCommittersInRepository(), gbc_lblCommittersInRepository);
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridwidth = 2;
			gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
			gbc_scrollPane.gridx = 0;
			gbc_scrollPane.gridy = 2;
			membersPanel.add(getScrollPane(), gbc_scrollPane);
			GridBagConstraints gbc_lblchooseTwoOr = new GridBagConstraints();
			gbc_lblchooseTwoOr.insets = new Insets(0, 0, 5, 5);
			gbc_lblchooseTwoOr.fill = GridBagConstraints.HORIZONTAL;
			gbc_lblchooseTwoOr.gridx = 0;
			gbc_lblchooseTwoOr.gridy = 3;
			membersPanel.add(getLblchooseTwoOr(), gbc_lblchooseTwoOr);
			GridBagConstraints gbc_btnMerge = new GridBagConstraints();
			gbc_btnMerge.fill = GridBagConstraints.BOTH;
			gbc_btnMerge.insets = new Insets(0, 0, 5, 0);
			gbc_btnMerge.gridx = 1;
			gbc_btnMerge.gridy = 3;
			membersPanel.add(getBtnMerge(), gbc_btnMerge);
			GridBagConstraints gbc_btnContinue = new GridBagConstraints();
			gbc_btnContinue.gridwidth = 2;
			gbc_btnContinue.fill = GridBagConstraints.BOTH;
			gbc_btnContinue.gridx = 0;
			gbc_btnContinue.gridy = 4;
			membersPanel.add(getBtnContinue(), gbc_btnContinue);
		}
		return membersPanel;
	}

	private JLabel getLblchooseTwoOr() {
		if (lblchooseTwoOr == null) {
			lblchooseTwoOr = new JLabel(
					"<html>*Choose two or more committers that represent the same team member. Repeat as many times needed. </html>");
		}
		return lblchooseTwoOr;
	}

	private JButton getBtnMerge() {
		if (btnMerge == null) {
			btnMerge = new JButton("Merge");
			btnMerge.setPreferredSize(new Dimension(63, 40));
			btnMerge.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					List<Integer> selectedMembersIndexes = new LinkedList<>();
					for (int i = 0; i < radioButtons.size(); i++) {
						if (radioButtons.get(i).isSelected()) {
							selectedMembersIndexes.add(i);
						}
					}
					Controller.mergeMembers(selectedMembersIndexes);
					updateScrollPane();
				}
			});
		}
		return btnMerge;
	}

	private void updateScrollPane() {
		committers.removeAll();
		radioButtons = new LinkedList<>();
		ActionListener al = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int counter = 0;
				for (JRadioButton jRadioButton : radioButtons) {
					if (jRadioButton.isSelected())
						counter++;
				}
				if (counter > 1) {
					btnMerge.setEnabled(true);
				} else {
					btnMerge.setEnabled(false);
				}
			}
		};
		for (TeamMember member : Controller.getCurrentTeam().getMembers()) {
			JRadioButton b = new JRadioButton("<html>" + member.getUserName() + "<br>" + member.getEmail() + "</html>");
			radioButtons.add(b);
			committers.add(b);
			b.addActionListener(al);
		}
		validate();
		repaint();
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getCommitters());
		}
		return scrollPane;
	}

	private JLabel getLblCommittersInRepository() {
		if (lblCommittersInRepository == null) {
			lblCommittersInRepository = new JLabel("Committers in repository:");
		}
		return lblCommittersInRepository;
	}

	private JButton getBtnContinue() {
		if (btnContinue == null) {
			btnContinue = new JButton("Create a report");
			btnContinue.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnContinue.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Controller.evaluateTeamSecondPhase();
					Controller.openReport(Controller.createReport());
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "end");
				}
			});
		}
		return btnContinue;
	}

	private JPanel getCommitters() {
		if (committers == null) {
			committers = new JPanel();
			committers.setLayout(new GridLayout(0, 1, 0, 0));
		}
		return committers;
	}

	private JPanel getEndPanel() {
		if (endPanel == null) {
			endPanel = new JPanel();
			GridBagLayout gbl_endPanel = new GridBagLayout();
			gbl_endPanel.columnWidths = new int[] { 634, 0 };
			gbl_endPanel.rowHeights = new int[] { 117, 60, 117, 40, 0 };
			gbl_endPanel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
			gbl_endPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
			endPanel.setLayout(gbl_endPanel);
			GridBagConstraints gbc_lblSuccess = new GridBagConstraints();
			gbc_lblSuccess.insets = new Insets(0, 0, 5, 0);
			gbc_lblSuccess.fill = GridBagConstraints.VERTICAL;
			gbc_lblSuccess.gridx = 0;
			gbc_lblSuccess.gridy = 1;
			endPanel.add(getLblSuccess(), gbc_lblSuccess);
			GridBagConstraints gbc_btnNewTeam = new GridBagConstraints();
			gbc_btnNewTeam.fill = GridBagConstraints.BOTH;
			gbc_btnNewTeam.gridx = 0;
			gbc_btnNewTeam.gridy = 3;
			endPanel.add(getBtnNewTeam(), gbc_btnNewTeam);
		}
		return endPanel;
	}

	private JLabel getLblSuccess() {
		if (lblSuccess == null) {
			lblSuccess = new JLabel("<html>Collaboration has been successfully assessed!");
			lblSuccess.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/success.png")));
		}
		return lblSuccess;
	}

	private JButton getBtnNewTeam() {
		if (btnNewTeam == null) {
			btnNewTeam = new JButton("Evaluate a new team/s");
			btnNewTeam.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnNewTeam.setFocusable(false);
			btnNewTeam.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					txtURI.setText(null);
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "begin");
				}
			});
		}
		return btnNewTeam;
	}

	private JPanel getBeginPanel() {
		if (beginPanel == null) {
			beginPanel = new JPanel();
			GridBagLayout gbl_beginPanel = new GridBagLayout();
			gbl_beginPanel.columnWidths = new int[] { 40, 270, 35, 270, 40, 0 };
			gbl_beginPanel.rowHeights = new int[] { 69, 160, 60, 50, 21, 0 };
			gbl_beginPanel.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
			gbl_beginPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
			beginPanel.setLayout(gbl_beginPanel);
			GridBagConstraints gbc_logoBig = new GridBagConstraints();
			gbc_logoBig.gridwidth = 5;
			gbc_logoBig.anchor = GridBagConstraints.NORTH;
			gbc_logoBig.insets = new Insets(0, 0, 5, 5);
			gbc_logoBig.gridx = 0;
			gbc_logoBig.gridy = 1;
			beginPanel.add(getLogoBig(), gbc_logoBig);
			GridBagConstraints gbc_btnEvaluateMultipleTeams = new GridBagConstraints();
			gbc_btnEvaluateMultipleTeams.fill = GridBagConstraints.BOTH;
			gbc_btnEvaluateMultipleTeams.insets = new Insets(0, 0, 5, 5);
			gbc_btnEvaluateMultipleTeams.gridx = 1;
			gbc_btnEvaluateMultipleTeams.gridy = 3;
			beginPanel.add(getBtnEvaluateMultipleTeams(), gbc_btnEvaluateMultipleTeams);
			GridBagConstraints gbc_btnEvaluateOneTeam = new GridBagConstraints();
			gbc_btnEvaluateOneTeam.insets = new Insets(0, 0, 5, 5);
			gbc_btnEvaluateOneTeam.fill = GridBagConstraints.BOTH;
			gbc_btnEvaluateOneTeam.gridx = 3;
			gbc_btnEvaluateOneTeam.gridy = 3;
			beginPanel.add(getBtnEvaluateOneTeam(), gbc_btnEvaluateOneTeam);
		}
		return beginPanel;
	}

	private JLabel getLogoBig() {
		if (logoBig == null) {
			logoBig = new JLabel("");
			logoBig.setPreferredSize(new Dimension(184, 160));
			logoBig.setHorizontalTextPosition(SwingConstants.CENTER);
			logoBig.setHorizontalAlignment(SwingConstants.CENTER);
			logoBig.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/logoveci.png")));
		}
		return logoBig;
	}

	private JButton getBtnEvaluateOneTeam() {
		if (btnEvaluateOneTeam == null) {
			btnEvaluateOneTeam = new JButton("Evaluate one team");
			btnEvaluateOneTeam.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnEvaluateOneTeam.setFocusable(false);
			btnEvaluateOneTeam.setAlignmentX(Component.CENTER_ALIGNMENT);
			btnEvaluateOneTeam.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "single");
				}
			});
		}
		return btnEvaluateOneTeam;
	}

	private JMenu getMenu_1() {
		if (menu == null) {
			menu = new JMenu("Help");
			menu.add(getMenuItem_1());
		}
		return menu;
	}

	private JMenuItem getMenuItem_1() {
		if (menuItem == null) {
			menuItem = new JMenuItem("About");
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Controller.openAboutDialog();
				}
			});
			menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
			menuItem.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/manual.png")));
		}
		return menuItem;
	}

	private JButton getBtnEvaluateMultipleTeams() {
		if (btnEvaluateMultipleTeams == null) {
			btnEvaluateMultipleTeams = new JButton("Evaluate multiple teams");
			btnEvaluateMultipleTeams.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnEvaluateMultipleTeams.setFocusable(false);
			btnEvaluateMultipleTeams.setAlignmentX(Component.CENTER_ALIGNMENT);
			btnEvaluateMultipleTeams.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "multiple");
				}
			});
		}
		return btnEvaluateMultipleTeams;
	}

	private JPanel getMultipleTeamsPanel() {
		if (multipleTeamsPanel == null) {
			multipleTeamsPanel = new JPanel();
			GridBagLayout gbl_multipleTeamsPanel = new GridBagLayout();
			gbl_multipleTeamsPanel.columnWidths = new int[] { 384, 0 };
			gbl_multipleTeamsPanel.rowHeights = new int[] { 60, 30, 193, 40, 0 };
			gbl_multipleTeamsPanel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
			gbl_multipleTeamsPanel.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
			multipleTeamsPanel.setLayout(gbl_multipleTeamsPanel);
			GridBagConstraints gbc_logoSmall = new GridBagConstraints();
			gbc_logoSmall.anchor = GridBagConstraints.SOUTH;
			gbc_logoSmall.insets = new Insets(0, 0, 5, 0);
			gbc_logoSmall.gridx = 0;
			gbc_logoSmall.gridy = 0;
			multipleTeamsPanel.add(getLogoSmall(), gbc_logoSmall);
			GridBagConstraints gbc_lblInputUrlsTo = new GridBagConstraints();
			gbc_lblInputUrlsTo.fill = GridBagConstraints.VERTICAL;
			gbc_lblInputUrlsTo.insets = new Insets(0, 0, 5, 0);
			gbc_lblInputUrlsTo.gridx = 0;
			gbc_lblInputUrlsTo.gridy = 1;
			multipleTeamsPanel.add(getLblInputUrlsTo(), gbc_lblInputUrlsTo);
			GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
			gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
			gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
			gbc_scrollPane_1.gridx = 0;
			gbc_scrollPane_1.gridy = 2;
			multipleTeamsPanel.add(getScrollPane_1(), gbc_scrollPane_1);
			GridBagConstraints gbc_btnEvaluateTeams = new GridBagConstraints();
			gbc_btnEvaluateTeams.fill = GridBagConstraints.BOTH;
			gbc_btnEvaluateTeams.gridx = 0;
			gbc_btnEvaluateTeams.gridy = 3;
			multipleTeamsPanel.add(getBtnEvaluateTeams(), gbc_btnEvaluateTeams);
		}
		return multipleTeamsPanel;
	}

	private JLabel getLogoSmall() {
		if (logoSmall == null) {
			logoSmall = new JLabel("");
			// label_2.setPreferredSize(new Dimension(115, 100));
			logoSmall.setHorizontalTextPosition(SwingConstants.CENTER);
			logoSmall.setHorizontalAlignment(SwingConstants.CENTER);
			logoSmall.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/logomali.png")));
		}
		return logoSmall;
	}

	private JLabel getLblInputUrlsTo() {
		if (lblInputUrlsTo == null) {
			lblInputUrlsTo = new JLabel("Input URIs to team repositories, each in a new line:");
		}
		return lblInputUrlsTo;
	}

	private JButton getBtnEvaluateTeams() {
		if (btnEvaluateTeams == null) {
			btnEvaluateTeams = new JButton("Evaluate teams and save reports");
			btnEvaluateTeams.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btnEvaluateTeams.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String text = textArea.getText();
					try {
						if (text.trim().isEmpty())
							throw new RuntimeException("Your input is not valid.");
						String[] uris = text.split("\n");
						JFileChooser chooser = new JFileChooser();
						chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						int value = chooser.showSaveDialog(contentPane);
						if (value == JFileChooser.APPROVE_OPTION) {
							CardLayout cl = (CardLayout) (contentPane.getLayout());
							cl.show(contentPane, "waiting");
							evaluateTeams(uris, chooser);
						}
					} catch (RuntimeException e) {
						JOptionPane.showMessageDialog(contentPane, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE,
								new ImageIcon(CollaborationInspectorGUI.class.getResource("/errors.png")));
					}

				}
			});
		}
		return btnEvaluateTeams;
	}

	public void evaluateTeams(String[] uris, JFileChooser chooser) {
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
			boolean finished = false;

			@Override
			protected Void doInBackground() {
				String report = "";
				for (int i = 0; i < uris.length; i++) {
					String uri = uris[i].trim();
					try {
						if (uri.isEmpty())
							continue;
						Controller.evaluateTeam(uri);
						int index = uri.lastIndexOf("/");
						String projectName = uri.substring(index);
						Controller.evaluateTeamSecondPhase();
						Controller.saveReport(chooser.getSelectedFile().getPath() + projectName + "_CI_REPORT",
								Controller.createReport());
						report += uri + " - successful evaluation\n";
					} catch (Exception e) {
						report += uri + " - " + e.getClass().toString() + ":" + e.getMessage() + "\n";
						e.printStackTrace();
					}
				}
				finished = true;
				JOptionPane.showMessageDialog(contentPane, report, "Batch of teams - report",
						JOptionPane.INFORMATION_MESSAGE,
						new ImageIcon(CollaborationInspectorGUI.class.getResource("/info.png")));
				return null;
			}

			@Override
			protected void done() {
				if (finished == true) {
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "end");
				} else {
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					cl.show(contentPane, "multiple");
					textArea.setText(null);
				}
			}
		};
		worker.execute();
	}

	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setViewportView(getTextArea());
		}
		return scrollPane_1;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setLineWrap(true);
		}
		return textArea;
	}

	@Override
	public JMenuBar getJMenuBar() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.setPreferredSize(new Dimension(0, 30));
			menuBar.add(getMenu_1());
			menuBar.add(Box.createHorizontalGlue());
			menuBar.add(getLblBack());

		}
		return menuBar;
	}

	private JLabel getLblBack() {
		if (lblBack == null) {
			lblBack = new JLabel("");
			lblBack.setVisible(false);
			lblBack.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			lblBack.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					CardLayout cl = (CardLayout) (contentPane.getLayout());
					if (membersPanel.isVisible()) {
						membersPanel.setVisible(false);
						cl.show(contentPane, "single");
					}

					else if (oneTeamPanel.isVisible() || multipleTeamsPanel.isVisible()) {
						txtURI.setText(null);
						textArea.setText(null);
						cl.show(contentPane, "begin");
					} else if (waitingPanel.isVisible()) {
						worker.cancel(false);
						cl.show(contentPane, "begin");
						txtURI.setText(null);
					}

				}
			});
			lblBack.setPreferredSize(new Dimension(40, 40));
			lblBack.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/back1.png")));
		}
		return lblBack;
	}
}
