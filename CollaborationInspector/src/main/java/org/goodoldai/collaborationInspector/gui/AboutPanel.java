package org.goodoldai.collaborationInspector.gui;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.UIManager;
import javax.swing.JEditorPane;

public class AboutPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9176744287097746925L;
	private JEditorPane dtrpncollaborationInspectorGives;

	/**
	 * Create the panel.
	 */
	public AboutPanel() {

		createContents();
	}

	private void createContents() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 300, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);
		GridBagConstraints gbc_dtrpncollaborationInspectorGives = new GridBagConstraints();
		gbc_dtrpncollaborationInspectorGives.fill = GridBagConstraints.BOTH;
		gbc_dtrpncollaborationInspectorGives.gridx = 0;
		gbc_dtrpncollaborationInspectorGives.gridy = 0;
		add(getDtrpncollaborationInspectorGives(), gbc_dtrpncollaborationInspectorGives);
	}

	private JEditorPane getDtrpncollaborationInspectorGives() {
		if (dtrpncollaborationInspectorGives == null) {
			dtrpncollaborationInspectorGives = new JEditorPane();
			dtrpncollaborationInspectorGives.setOpaque(false);
			dtrpncollaborationInspectorGives.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
			dtrpncollaborationInspectorGives.setFont(UIManager.getFont("Menu.font"));
			dtrpncollaborationInspectorGives.setEditable(false);
			dtrpncollaborationInspectorGives.setContentType("text/html");
			dtrpncollaborationInspectorGives.setText(
					"<html><br><b>Collaboration inspector</b> is a fuzzy expert system that gives recommendations for assignment of digital badges to students working in teams of three based on their collaboration skills. The team project analysed is a programming project written in Java. There are three levels of collaboration, each indicating a more profound understanding of teamwork than the last. \r\n<br><br>\r\nFirst level badge is <b>bronze</b> and signifies that a team member made a contribution to the project and is liable for the outcome of the project. \r\n<br><br>\r\nSecond level badge is <b>silver</b> and signifies that a team member has a role/assignment in the team project which points to joint decision making being a part of project development. \r\n<br><br>\r\nThird level badge is <b>gold</b> and signifies that a team member has done a fair amount of work in regard to the rest of the team members and that his work is essential for the project and dependant on the rest of the project.\r\n<br><br>\r\nFuzzy system evaluates quantitatively expressed requirements and gives its recommendations in a form of an <b>HTML report</b>. The report contains all pertinent information regarding the conclusion reached by the system.\r\n<br><br>\r\nAuthor: Anisja Kijev\u010Danin\r\n</html>");
		}
		return dtrpncollaborationInspectorGives;
	}
}
