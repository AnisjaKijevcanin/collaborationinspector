package org.goodoldai.collaborationInspector.gui;

import java.awt.BorderLayout;
import java.awt.Desktop;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import javax.swing.UIManager;

public class ReportDialog extends JDialog {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8824177124732759758L;
	/**
	 * Create the dialog.
	 */
	public ReportDialog(String html) {
		createContents(html);
		
	}
	private void createContents(String html) {
		setTitle("Team report");
		setResizable(false);
		setBounds(100, 100, 1080, 720);
		getContentPane().setLayout(new BorderLayout());
		{
			JMenuBar menuBar = new JMenuBar();
			menuBar.setPreferredSize(new Dimension(0, 30));
			setJMenuBar(menuBar);
			{
				JMenu mnFile = new JMenu("File");
				menuBar.add(mnFile);
				{
					JMenuItem mntmSave = new JMenuItem("Save");
					mntmSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
					mntmSave.setIcon(new ImageIcon(CollaborationInspectorGUI.class.getResource("/save.png")));
					mntmSave.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							JFileChooser chooser = new JFileChooser();
							chooser.setSelectedFile(new File(Controller.getCurrentTeam().getUri() + "_CI_Report"));
							int value = chooser.showSaveDialog(mntmSave);
							if (value == JFileChooser.APPROVE_OPTION) {
								Controller.saveReport(chooser.getSelectedFile().getPath(), html);
								dispose();
							}
						}
					});
					mnFile.add(mntmSave);
				}
			}
		}
		{
			JScrollPane scrollPane = new JScrollPane();
			getContentPane().add(scrollPane, BorderLayout.CENTER);
			{
				JEditorPane editorPane = new JEditorPane();
				editorPane.setOpaque(false);
				editorPane.setBackground(UIManager.getColor("Panel.background"));
				editorPane.setEditable(false);
				editorPane.setContentType("text/html");
				scrollPane.setViewportView(editorPane);
				int findex = html.indexOf("<header>");
				int lindex = html.indexOf("<div id=\"content\">");
				String header = html.substring(findex, lindex);
				editorPane.addHyperlinkListener(new HyperlinkListener() {
					
					@Override
					public void hyperlinkUpdate(HyperlinkEvent e) {
						try {
							if(e.getEventType().equals(EventType.ACTIVATED))
								Desktop.getDesktop().browse(e.getURL().toURI());
						} catch (IOException e1) {
							e1.printStackTrace();
						} catch (URISyntaxException e1) {
							e1.printStackTrace();
						}						
					}
					
					
				});
				String changedHtml = html.replace("#wrapper {\r\n" + 
						"	width: 80%;\r\n" + 
						"	margin-left: 10%;\r\n" + 
						"	margin-right: 10%;\r\n" + 
						"	background: white;\r\n" + 
						"	min-height: 100%;\r\n" + 
						"}", "#wrapper{background: #ccebff;}");
				editorPane.setText(changedHtml.replace(header, "<h2 text-align: \"center\"> Team report: " + Controller.getCurrentTeam().getUri() + "</h2>"));
				editorPane.setCaretPosition(0);
			}
		}
	}

}
