package org.goodoldai.collaborationInspector.gui;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.goodoldai.collaborationInspector.assessment.BadgesCalculate;
import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.operations.CalculateCommits;
import org.goodoldai.collaborationInspector.operations.CalculateLinesOfCode;
import org.goodoldai.collaborationInspector.operations.CalculatePercentagesPerMember;
import org.goodoldai.collaborationInspector.operations.CheckDates;
import org.goodoldai.collaborationInspector.operations.InitializeRepositoryAndTeam;
import org.goodoldai.collaborationInspector.operations.MergeMembers;
import org.goodoldai.collaborationInspector.report.CreateTeamReport;

public class Controller {
	private static CollaborationInspectorGUI mainWindow;
	private static Team currentTeam;

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		EventQueue.invokeLater(() -> {
			mainWindow = new CollaborationInspectorGUI();
			mainWindow.setVisible(true);
			mainWindow.setLocationRelativeTo(null);
		});

	}

	public static void closeTheApplication() {
		int response = JOptionPane.showConfirmDialog(mainWindow, "Do you want to close the application?", "Closing",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
				new ImageIcon(Controller.class.getResource("/question.png")));
		if (response == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	public static void openAboutDialog() {
		JDialog dialog = new JDialog(mainWindow, "About", false);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setContentPane(new AboutPanel());
		dialog.setSize(650, 450);
		dialog.setVisible(true);
		dialog.setResizable(false);
		dialog.setLocationRelativeTo(mainWindow);
	}

	public static void openReport(String dok) {
		ReportDialog dialog = new ReportDialog(dok);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
		dialog.setLocationRelativeTo(null);
	}

	public static Team evaluateTeam(String uri) throws Exception {
		currentTeam = InitializeRepositoryAndTeam.execute(uri);
		CalculateCommits.execute(currentTeam);
		CalculateLinesOfCode.execute(currentTeam);
		return currentTeam;
	}

	public static void evaluateTeamSecondPhase() {
		CalculatePercentagesPerMember.execute(currentTeam);
		CheckDates.execute(currentTeam);
		BadgesCalculate.execute(currentTeam);
	}

	public static void saveReport(String file, String html) {
		PrintWriter pw;
		try {
			pw = new PrintWriter(new BufferedWriter(new FileWriter(file + ".html")));
			pw.print(html);
			pw.close();
			File htmlFile = new File(file + ".html");
			Desktop.getDesktop().browse(htmlFile.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String createReport() {
		return CreateTeamReport.execute(currentTeam);
	}

	public static void mergeMembers(List<Integer> members) {
		MergeMembers.execute(currentTeam, members);
	}

	public static Team getCurrentTeam() {
		return currentTeam;
	}

	public static void setCurrentTeam(Team currentTeam) {
		Controller.currentTeam = currentTeam;
	}

	public static CollaborationInspectorGUI getMainWindow() {
		return mainWindow;
	}

}
