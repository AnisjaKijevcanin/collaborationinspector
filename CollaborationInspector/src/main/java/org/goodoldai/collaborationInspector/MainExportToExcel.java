package org.goodoldai.collaborationInspector;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.goodoldai.collaborationInspector.assessment.BadgesCalculate;
import org.goodoldai.collaborationInspector.core.Team;
import org.goodoldai.collaborationInspector.operations.CalculateCommits;
import org.goodoldai.collaborationInspector.operations.CalculateLinesOfCode;
import org.goodoldai.collaborationInspector.operations.CalculatePercentagesPerMember;
import org.goodoldai.collaborationInspector.operations.CheckDates;
import org.goodoldai.collaborationInspector.operations.InitializeRepositoryAndTeam;

public class MainExportToExcel {

	public static void main(String[] args) {
		try {
			//out stream for filling the xls document
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("resultsAll20172018.xls")));
			out.print("Team\tUsername\tTotal commits\tLinesAdded\tLinesDeleted\tPercentage documenting\t"
					+ "Percentage GUI\tPercentage testing\tPercentage domain classes\tPercentage total added\tPercentage total deleted"
					+ "\tNumber of appearances\tBB\tSB\tGB\tBronze badge\tSilver badge\tGold badge\t" );
			out.println();
			for (int i = 1; i <= 20; i++) {
				System.out.println("\nTeam number " + i);
				Team team = null;
				try {
					team = InitializeRepositoryAndTeam.execute("https://github.com/JGRASS/project" + i + "-2017");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(team == null) {
					 continue;
				}
				
				CalculateCommits.execute(team);
				CalculateLinesOfCode.execute(team);
				CalculatePercentagesPerMember.execute(team);
				CheckDates.execute(team);
				BadgesCalculate.execute(team);
				for (int j = 0; j < team.getMembers().size(); j++) {
				 	System.out.println(team.getMembers().get(j).toString());
				 	out.print(i + "\t");
				 	out.print(team.getMembers().get(j).getUserName() + "\t");
				 	out.print(team.getMembers().get(j).getTotalCommits() + "\t");
				 	out.print(team.getMembers().get(j).getCommitInformationTotal().getLinesAdded() + "\t");
				 	out.print(team.getMembers().get(j).getCommitInformationTotal().getLinesDeleted() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageDoc() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageGUI() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageTest() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageDomainClasses() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageTotalAdded() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageTotalDeleted() + "\t");
				 	out.print(team.getMembers().get(j).getNumberOfAppearances() + "\t");
				 	out.print(team.getMembers().get(j).getBronzeBadgeDouble()+ "\t");
				 	out.print(team.getMembers().get(j).getSilverBadgeDouble()+ "\t");
				 	out.print(team.getMembers().get(j).getGoldBadgeDouble()+ "\t");
				 	out.print(team.getMembers().get(j).isBronzeBadge()+ "\t");
				 	out.print(team.getMembers().get(j).isSilverBadge()+ "\t");
				 	out.print(team.getMembers().get(j).isGoldBadge()+ "\t");
				 	out.println();
				
				}
			}
			for (int k = 1; k <= 22; k++) {
				System.out.println("\nTeam number " + k);
				Team team = null;
				try {
					team = InitializeRepositoryAndTeam.execute("https://github.com/JGRASS/project" + k + "-2018");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(team == null) {
					 continue;
				}
				
				CalculateCommits.execute(team);
				CalculateLinesOfCode.execute(team);
				CalculatePercentagesPerMember.execute(team);
				CheckDates.execute(team);
				BadgesCalculate.execute(team);
				for (int j = 0; j < team.getMembers().size(); j++) {
				 	System.out.println(team.getMembers().get(j).toString());
				 	out.print(k + "\t");
				 	out.print(team.getMembers().get(j).getUserName() + "\t");
				 	out.print(team.getMembers().get(j).getTotalCommits() + "\t");
				 	out.print(team.getMembers().get(j).getCommitInformationTotal().getLinesAdded() + "\t");
				 	out.print(team.getMembers().get(j).getCommitInformationTotal().getLinesDeleted() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageDoc() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageGUI() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageTest() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageDomainClasses() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageTotalAdded() + "\t");
				 	out.print(team.getMembers().get(j).getPercentageTotalDeleted() + "\t");
				 	out.print(team.getMembers().get(j).getNumberOfAppearances() + "\t");
				 	out.print(team.getMembers().get(j).getBronzeBadgeDouble()+ "\t");
				 	out.print(team.getMembers().get(j).getSilverBadgeDouble()+ "\t");
				 	out.print(team.getMembers().get(j).getGoldBadgeDouble()+ "\t");
				 	out.print(team.getMembers().get(j).isBronzeBadge()+ "\t");
				 	out.print(team.getMembers().get(j).isSilverBadge()+ "\t");
				 	out.print(team.getMembers().get(j).isGoldBadge()+ "\t");
				 	out.println();
				}
			}	

			out.close();
			} catch (InvalidRemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransportException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (GitAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}

	}


