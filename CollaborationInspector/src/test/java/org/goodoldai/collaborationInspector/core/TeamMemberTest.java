/**
 * 
 */
package org.goodoldai.collaborationInspector.core;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * org.goodoldai.collaborationInspector.core.TeamMember class tests
 * 
 * @author Bojan Tomic
 * @see org.goodoldai.collaborationInspector.core.TeamMember
 *
 */
public class TeamMemberTest {
	
	private TeamMember instance = null;
	private LinkedList<String> alternativeEmails;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		instance = new TeamMember();
		alternativeEmails = new LinkedList<String>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		instance = null;
		alternativeEmails = null;
	}

	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setUserName(java.lang.String)}.
	 */
	@Test
	public void testSetUserNameAllOk() {
		String userName = "Nick";
		
		instance.setUserName(userName);
		
		assertEquals(userName, instance.getUserName());
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setUserName(java.lang.String)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetUserNameNull() {
		instance.setUserName(null);
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setUserName(java.lang.String)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetUserNameEmptyString() {
		instance.setUserName("");
	}

	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setEmail(java.lang.String)}.
	 */
	@Test
	public void testSetEmailAllOk() {
		String email = "john@doe.net";
		
		instance.setEmail(email);
		
		assertEquals(email, instance.getEmail());
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setEmail(java.lang.String)}.
	 */
	@Test
	public void testSetEmailAllOk2() {
		String email = "j@d";
		
		instance.setEmail(email);
		
		assertEquals(email, instance.getEmail());
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setEmail(java.lang.String)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetEmailNull() {
		instance.setEmail(null);
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setEmail(java.lang.String)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetEmailLessThanThreeChars() {
		instance.setEmail("2@");
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setEmail(java.lang.String)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetEmailLessThanThreeChars2() {
		instance.setEmail("j");
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setEmail(java.lang.String)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetEmailLessThanThreeChars3() {
		instance.setEmail("");
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setEmail(java.lang.String)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetEmailNoAtChar() {
		instance.setEmail("johndoe.net");
	}


	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setTotalCommits(int)}.
	 */
	@Test
	public void testSetTotalCommitsAllOK() {
		instance.setTotalCommits(0);
		
		assertEquals(0, instance.getTotalCommits());
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setTotalCommits(int)}.
	 */
	@Test
	public void testSetTotalCommitsAllOK2() {
		instance.setTotalCommits(13);
		
		assertEquals(13, instance.getTotalCommits());
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.TeamMember#setTotalCommits(int)}.
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamMemberException.class)
	public void testSetTotalCommitsNegtiveCommits() {
		instance.setTotalCommits(-1);
	}
	

}
