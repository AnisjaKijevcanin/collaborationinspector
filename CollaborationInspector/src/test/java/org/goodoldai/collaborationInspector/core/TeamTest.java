/**
 * 
 */
package org.goodoldai.collaborationInspector.core;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.RepositoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bojan
 *
 */
public class TeamTest {
	
	private Team instance = null;
	private LinkedList<TeamMember> members = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		instance = new Team();
		members = new LinkedList<TeamMember>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		instance = null;
		members = null;
	}

	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.Team#setRepository(org.eclipse.jgit.lib.Repository)}.
	 * @throws IOException 
	 */
	@Test
	public void testSetRepository() throws IOException {
		Repository repository = new RepositoryBuilder() //
                .setGitDir(new File("")) // --git-dir if supplied, no-op if null
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .build();
		
		instance.setRepository(repository);
		
		assertEquals(repository, instance.getRepository());
		
		repository.close();
	}
	
	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.Team#setRepository(org.eclipse.jgit.lib.Repository)}.
	 * @throws IOException 
	 */
	@Test (expected = org.goodoldai.collaborationInspector.exceptions.TeamException.class)
	public void testSetRepositoryNull() throws IOException {
		instance.setRepository(null);
	}

	/**
	 * Test method for {@link org.goodoldai.collaborationInspector.core.Team#setMembers(java.util.LinkedList)}.
	 */
	@Test
	public void testSetMembers() {
		TeamMember member1 = new TeamMember();
		member1.setUserName("John");

		TeamMember member2 = new TeamMember();
		member2.setUserName("Jane");
		
		members.add(member1);
		members.add(member2);
		
		instance.setMembers(members);
		
		assertEquals(members, instance.getMembers());
	
	}

}
